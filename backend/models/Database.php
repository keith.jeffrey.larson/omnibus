<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 04:29
 */

class Database {

    /**
     * Connects to database and returns a PDO database object
     * @return PDO|string
     */
    public static function Get()
    {
        $path = dirname(__DIR__, 3);
        $config = parse_ini_file($path.'/config.ini');

        $host = $config['host'];
        $name = $config['name'];
        $user = $config['user'];
        $pass = $config['pass'];

        // Try connecting to MySQL
        try {
            $dbh = new PDO("mysql:host=$host;dbname=$name", $user, $pass);
        } catch (PDOException $e) {
            return 'Database error!: [1]' . $e->getMessage();
        }

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $dbh;
    }
}
