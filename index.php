<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 28.09.2018
 * Time: 03:03
 */

use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_name('__Secure-PHPSESSID');
session_set_cookie_params(
    0,
    '/',
    'sfnw.online',
    true,
    true
);
session_start();
session_regenerate_id(true);

require 'vendor/autoload.php';
require_once 'backend/models/controller-manager.php';
require __DIR__.'/backend/Token.php';
require __DIR__.'/backend/Settings.php';

// Set up router
$router = new AltoRouter();
$router->setBasePath('');
$router->addMatchTypes(array('f' => '[a-zA-Z0-9\-]++'));

// Set up Whoops
$whoops = new Run;
$whoops->pushHandler(new PrettyPageHandler);
$whoops->register();

// Check RememberMe
$remember_cookie = $_COOKIE['__Secure-rememberme'] ?? '';
if ($remember_cookie) {
    [$user, $token, $mac] = explode(':', $remember_cookie);
    if (!password_verify($user . ':' . $token, $mac)) {
        die('Invalid!');
    }
    $usertoken = User::GetRememberMe($user);
    if (hash_equals($usertoken, $token)) {
        $_SESSION['userid'] = $user;
    }
}

// Get user
$user = !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null;
// Get user role
$role = !empty($_SESSION['userid']) ? $user->role : null;


// Update last seen time
if (!empty($_SESSION['userid'])) {
    User::UpdateLastSeen($user->id);
}


/* *********************************************** *\
|*                  ROUTE MAPPINGS                 *|
\* *********************************************** */

/***************************************************************\
*                     PUBLIC ACCESS ROUTES                      *
*              Parts of the site anyone can access              *
\***************************************************************/
try {
    $router->addRoutes(array(
        //Home
        array('GET', '/', static function () {require 'public/controllers/Home.php';}, 'home'),
        // Article
        array('GET', '/article/[i:id]/[f:furl]?', static function ($id) {$id; require 'public/controllers/Article.php';}),
        array('GET', '/a/[:id]/[f:furl]?', static function ($id) {$id; require 'public/controllers/Article.php';}),
        // Articles
        array('GET', '/articles/[:p]?', static function ($p=1) {$p; require 'public/controllers/Articles.php';}),
        // Categories
        array('GET', '/categories', static function () {require 'public/controllers/Categories.php';}),
        array('GET', '/category/[i:id]/[f:furl]?', static function ($id) {$id; require 'public/controllers/Categories.php';}),
        array('GET', '/c/[:id]/[f:furl]?', static function ($id) {$id; require 'public/controllers/Categories.php';}),
        // Categories
        array('GET', '/tags', static function () {require 'public/controllers/Tags.php';}),
        array('GET', '/tag/[i:id]/[f:furl]?', static function ($id) {$id; require 'public/controllers/Tags.php';}),
        array('GET', '/t/[i:id]/[f:furl]?', static function ($id) {$id; require 'public/controllers/Tags.php';}),
        // Authors
        array('GET', '/authors', static function () {require 'public/controllers/Authors.php';}),
        array('GET', '/author/[i:id]/[f:furl]?', static function ($id) {$id; require 'public/controllers/Authors.php';}),
        array('GET', '/au/[i:id]/[f:furl]?', static function ($id) {$id; require 'public/controllers/Authors.php';}),
        // Classes
        array('GET', '/class/[i:id]?', static function ($id=null) {$id; require 'public/controllers/Classes.php';}),

        // USERS
        // Login
        array('GET|POST', '/login', static function () {require 'public/controllers/User/Login.php';}),
        // Logout
        array('GET', '/logout', static function () {require 'public/controllers/User/Logout.php';}),
        // Register
        array('GET|POST', '/register', static function () {require 'public/controllers/User/Register.php';}),
        // Activate
        array('GET', '/activate/[a:code]?', static function ($code) {$code; require 'public/controllers/User/Activate.php';}),
        // Restore
        array('GET|POST', '/restore', static function () {require 'public/controllers/User/Restore.php';}),
    ));
} catch (Exception $e) {
    echo $e;
}

/***************************************************************\
 *                        USER-ONLY AREA                        *
 *          Area only registered users have access to           *
\***************************************************************/
if ($user !== null) {
    try {
        $router->addRoutes(array(
            //User's own profile
            array('GET', '/profile', static function () { require 'public/controllers/User/Profile/Profile.php'; }),
            // Profile edit
            array('GET|POST', '/profile/update', static function () { require 'public/controllers/User/Profile/Update.php'; }),
            array('GET|POST', '/profile/edit', static function () { require 'public/controllers/User/Profile/Edit.php'; }),
            // User profiles
            array('GET', '/user/[i:id]/[f:furl]?', static function ($id) { $id; require 'public/controllers/User/Profile/Profile.php'; }),
            array('GET', '/u/[i:id]/[f:furl]?', static function ($id) { $id; require 'public/controllers/User/Profile/Profile.php'; }),
        ));
    } catch (Exception $e) {
        echo $e;
    }
}

/***************************************************************\
*                     ADMIN-ONLY ROUTES                         *
*  Parts of the site only those with role->is_admin can access  *
\***************************************************************/
if ($role !== null && $role->is_admin) {
    try {
        $router->addRoutes(array(
            // ADMIN
            array('GET', '/admin', static function () {require 'public/controllers/Admin/Dashboard.php';}),
            // Managers
            array('GET', '/admin/articles', static function () {require 'public/controllers/Admin/Managers/Articles.php';}),
            array('GET', '/admin/users', static function () {require 'public/controllers/Admin/Managers/Users.php';}),
            array('GET', '/admin/categories', static function () {require 'public/controllers/Admin/Managers/Categories.php';}),
            array('GET', '/admin/tags', static function () {require 'public/controllers/Admin/Managers/Tags.php';}),
            array('GET', '/admin/classes', static function () {require 'public/controllers/Admin/Managers/Classes.php';}),
            array('GET', '/admin/comments', static function () {require 'public/controllers/Admin/Managers/Comments.php';}),
            // Editors
            array('GET', '/admin/articles/edit/[i:id]?', static function ($id = null) {$id; require 'public/controllers/Admin/Editors/Article.php';}),
            array('POST', '/admin/articles/edit', static function () {require 'public/controllers/Admin/Editors/Article.php';}),
            array('GET', '/admin/categories/edit/[i:id]?', static function ($id = null) {$id; require 'public/controllers/Admin/Editors/Category.php';}),
            array('POST', '/admin/categories/edit', static function () {require 'public/controllers/Admin/Editors/Category.php';}),
            array('GET', '/admin/tags/edit/[i:id]?', static function ($id = null) {$id; require 'public/controllers/Admin/Editors/Tag.php';}),
            array('POST', '/admin/tags/edit', static function () {require 'public/controllers/Admin/Editors/Tag.php';}),
            array('GET', '/admin/users/edit/[i:id]?', static function ($id = null) {$id; require 'public/controllers/Admin/Editors/User.php';}),
            array('POST', '/admin/users/edit', static function () {require 'public/controllers/Admin/Editors/User.php';}),
            array('GET', '/admin/classes/edit/[i:id]?', static function ($id = null) {$id; require 'public/controllers/Admin/Editors/Class.php';}),
            array('POST', '/admin/classes/edit', static function () {require 'public/controllers/Admin/Editors/Class.php';}),
            // Fixer
//            array('GET|POST', '/fix', function () {require 'public/controllers/_fix.php';}),

        ));
    } catch (Exception $e) {
        echo $e;
    }
}

/***************************************************************\
 *                       ROUTE TO MATRIX                         *
 *  Parts of the site only those with role->is_admin can access  *
\***************************************************************/
if ($role !== null && $role->matrix_access) {
    try {
        $router->addRoutes(array(
            // Matrix
            array('GET|POST', '/admin/articles/matrix', static function (){require 'public/controllers/Admin/Managers/Matrix.php';}),
        ));
    } catch (Exception $e) {
        echo $e;
    }
}

/***************************************************************\
*                        SERVICE ROUTES                         *
*         Routes meant to be accessed by external means         *
*                 RSS, API, CRON, sitemap, etc                  *
\***************************************************************/
try {
    $router->addRoutes(array(
        // Rss
        array('GET', '/RSS', static function () {require 'public/controllers/Utility/Rss.php';}),
    ));
} catch (Exception $e) {
    echo $e;
}


/* *********************************************** *\
|*                  ROUTE HANDLING                 *|
\* *********************************************** */

$match = $router->match();

// Handle routing
if( $match && is_callable( $match['target'] ) ) {
    call_user_func_array( $match['target'], $match['params'] );
} else {
    // no route was matched
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    die('404');
}
