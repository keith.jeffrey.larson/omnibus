<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 22.10.2018
 * Time: 04:42
 */

/** @var $t_get_icon Twig_SimpleFunction */
$t_get_icon = new Twig_SimpleFunction('get_icon', static function ($url) {

    if ($url === '' || empty($url)) {
        return null;
    }

    $host = str_replace('www.', '', parse_url($url, PHP_URL_HOST));

    switch ($host) {
        case 'twitter.com':
            return'https://sfnw.online/public/assets/icons/twitter.png';
            break;

        case 'tumblr.com':
            return'https://sfnw.online/public/assets/icons/tumblr.png';
            break;

        case 'fimfiction.net':
            return'https://sfnw.online/public/assets/icons/fimfiction.png';
            break;

        case 'archiveofourown.org':
            return'https://sfnw.online/public/assets/icons/ao3.png';
            break;

        case 'fanfiction.net':
            return'https://sfnw.online/public/assets/icons/fanfiction.png';
            break;

        default:
            return'https://sfnw.online/public/assets/icons/link.png';
            break;
    }
});

/** @var $t_get_name Twig_SimpleFunction */
$t_get_name = new Twig_SimpleFunction('get_name', static function ($url) {

    if ($url === '' || empty($url)) {
        return null;
    }

    $host = str_replace('www.', '', parse_url($url, PHP_URL_HOST));

    switch ($host) {
        case 'twitter.com':
            return 'Twitter';
            break;

        case 'tumblr.com':
            return 'Tumblr';
            break;

        case 'fimfiction.net':
            return 'FiMFiction';
            break;

        case 'archiveofourown.org':
            return 'AO3';
            break;

        case 'fanfiction.net':
            return 'Fanfiction.net';
            break;

        default:
            return 'Link';
            break;
    }
});
