<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 29.09.2018
 * Time: 02:24
 */

if(!empty($_SESSION['userid'])) {
    header('Location: /');
    die();
}

require dirname(__DIR__, 3).'/backend/Captcha.php';
require dirname(__DIR__, 3).'/backend/TwigExtensions/markdown.php';

// LOGIN USER
$error = false;
$message = array(
    'type'   => 'error',
    'header' => 'Error!',
    'body'   => ''
);
$issues = array();
if (!empty($_POST) && !empty($_SESSION)) {

    // Validate tokens
    if ($_POST['token'] !== $_SESSION['token']) {
        die('Access this form from website only.');
    }

    // Check $_POST keys
    $whitelist = array('login', 'password', 'email', 'token', 'privacy', 'terms', 'g-recaptcha-response');
    foreach ($_POST as $key=>$item) {
        if (!in_array($key, $whitelist, false)) {
            die('Please use only the fields in the form.');
        }
    }

    // check secret key
    $captcha = new Captcha(Settings::Read('captcha-secret'));
    $response = $captcha->Verify($_POST['g-recaptcha-response']);

    // Get gravatar
    if ($response === true) {

        // Check if fields aren't empty
        if (empty($_POST['login'])) {
            $error = true;
            $message['body'] .= '⚠ Empty login<br>';
        }
        if (empty($_POST['password'])) {
            $error = true;
            $message['body'] .= '⚠ Empty password<br>';
        }
        if (empty($_POST['email'])) {
            $error = true;
            $message['body'] .= '⚠ Empty email<br>';
        }

        // Check if emails are valid
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $error = true;
            $message['body'] .= '⚠ Invalid email address<br>';
        }

        // Check if user exists
        if (User::GetByName($_POST['login']) !== null) {
            $error = true;
            $message['body'] .= '⚠ Name has been taken<br>';
        }
        if (User::GetByEmail($_POST['email']) !== null) {
            $error = true;
            $message['body'] .= '⚠ Email already exists<br>';
        }

        // If everything's fine, add to database
        if(!$error) {
            $code = substr(md5(mt_rand()), 0, 15);
            User::Add($_POST['login'], $_POST['password'], $_POST['email'], $code);

            $body = file_get_contents(dirname(__DIR__, 2).'/assets/emails/welcome.html');
            $body = str_replace(array('{{user}}', '{{code}}'), array($_POST['login'], $code), $body);
            $subject = 'School for New Writers activation code';
            $headers = @"From: \"The Dean of SFNW\" <dean@sfnw.online>\r\n".
                @"Reply-To: \"The Dean of SFNW\" <dean@sfnw.online>\r\n".
                @'X-Mailer: PHP/' . PHP_VERSION .
                @"MIME-Version: 1.0\r\n".
                @"Content-Type: text/html; charset=ISO-8859-1\r\n";

            try {
                mail($_POST['email'], $subject, $body, $headers);
            } catch (Exception $e) {
                echo $e;
            }

            header('Location: /?msg=registered');
        }

    } else {
        // redirect if captcha incorrect
        $message['body'] .= '⚠ Incorrect Captcha<br>';
    }
}

// Load up Twig stuff
require dirname(__DIR__, 3).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$token = Token::Get(64);
$_SESSION['token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('/user/register.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null,
        'discord_users' => Discord::Online(),
        'quote'         => Quote::GetRandomQuote(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),

        'token'         => $token,
        'message'       => $message,

        'privacy'        => file_get_contents(dirname(__DIR__, 2).'/assets/flatfiles/legal/privacy.md'),
        'terms'          => file_get_contents(dirname(__DIR__, 2).'/assets/flatfiles/legal/terms.md'),
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}
