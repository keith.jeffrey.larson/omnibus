Cookies
=======

## Necessary
Necessary for the site to function properly

### _cfuid
**Provider:** cloudflare.com\
**Type:** HTTP\
**Expiry:** 1 year\
**Purpose:** Used by the content network, Cloudflare, to identify trusted web traffic.

### __Secure-PHPSESSID
**Provider:** sfnw.online\
**Type:** HTTP\
**Expiry:** Session\
**Purpose:** Contains a session identifier that allows the user to log in

## User
Ones user opt-ins through taking an action

### __Secure-rememberme
**Provider:** sfnw.online\
**Type:** HTTP\
**Expiry:** 1 month\
**Purpose:** Contains a token that allows the user to stay logged in even after the browser window closes
**Action:** Ticking a "remember me" checkbox during login

### theme
**Provider:** sfnw.online\
**Type:** HTTP\
**Expiry:** Thu, 01 Jan 2100 00:00:00 UTC\
**Purpose:** Contains an identifier of the currently selected theme\
**Action:** Changing the website theme

## Tracking
Every cookie necessary for tracking traffic on the site, provided by Google Analytics

### _ga
**Provider:** sfnw.online\
**Type:** HTTP\
**Expiry:** 2 years\
**Purpose:** Registers a unique ID that is used by Google analytics to generate statistical data on how the visitor uses the website.

### _gat
**Provider:** sfnw.online\
**Type:** HTTP\
**Expiry:** Session\
**Purpose:** Used by Google Analytics to throttle request rate

### _gid
**Provider:** sfnw.online\
**Type:** HTTP\
**Expiry:** Session\
**Purpose:** Registers a unique ID that is used by Google Analytics to generate statistical data on how the visitor uses the website.

### collect
**Provider:** google-analytics.com\
**Type:** Pixel\
**Expiry:** Session\
**Purpose:** Used to send data to Google Analytics about the visitor's device and behaviour. Tracks the visitor across
             devices and marketing channels.
