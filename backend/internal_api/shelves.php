<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 30.11.2018
 * Time: 07:26
 */

session_name('__Secure-PHPSESSID');
session_set_cookie_params(
    0,
    '/',
    'sfnw.online',
    true,
    true
);
session_start();

require dirname(__DIR__).'/models/Shelf.php';

//Receive the RAW post data.
$content = trim(file_get_contents('php://input'));
//Attempt to decode the incoming RAW post data from JSON.
$data = json_decode($content, true);

if ($_SESSION['fetch_token'] === $data['fetch_token']) {

    $msg = '';
    $item = '';
    if ($data['shelf'] === 'b') {
        $marked = Shelf::CheckBookmark($data['article'], $_SESSION['userid']);
        $item = 'bookmark';
        if ($marked) {
            Shelf::RemoveBookmark($data['article'], $_SESSION['userid']);
            $msg = 'removed';
        } else {
            Shelf::AddBookmark($data['article'], $_SESSION['userid']);
            $msg = 'added';
        }
    } else if ($data['shelf'] === 'f') {
        $marked = Shelf::CheckFavourite($data['article'], $_SESSION['userid']);
        $item = 'favourite';
        if ($marked) {
            Shelf::RemoveFavourite($data['article'], $_SESSION['userid']);
            $msg = 'removed';
        } else {
            Shelf::AddFavourite($data['article'], $_SESSION['userid']);
            $msg = 'added';
        }
    }
} else {
    header('HTTP/2.0 500 Forbidden');
    die(json_encode(array(
        $data['fetch_token']
    )));
}

header('HTTP/2.0 200 OK');
header('Content-Type: application/json');
echo json_encode(array(
    'item' => $item,
    'msg' => $msg,
));
die();
