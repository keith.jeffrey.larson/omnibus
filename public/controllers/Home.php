<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 28.09.2018
 * Time: 02:13
 */

// Load up Twig stuff
require dirname(__DIR__, 2).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$user = !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null;
$token = Token::Get(32);
$_SESSION['fetch_token'] = $token;

// Set up message, if necessary
$message = array();
if (isset($_GET['msg'])) {
    switch ($_GET['msg']) {
        case 'login':
            $message = array(
                'type' => 'success timed',
                'header' => 'Hello, ' . $user->name . '!',
                'body' => 'You have been logged in successfully',
            );
            break;
        case 'logout':
            $message = array(
                'type' => 'success timed',
                'header' => 'See you soon!',
                'body' => 'You have been logged out successfully',
            );
            break;
        case 'registered':
            $message = array(
                'type' => 'success',
                'header' => 'Glad you\'re with us!',
                'body' => 'Your account has been created successfully!<br>Now you only need to <a href="/activate">activate it</a> – expect an email shortly.',
            );
            break;

    }
}

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('home.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => $user,
        'discord_users' => Discord::Online(),
        'quote'         => Quote::GetRandomQuote(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),
        'message'       => $message,
        'token'         => $token,

        'articles'      => Article::ParseAll(Article::GetAll(10)),
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
} catch (Exception $e) {
    header('Content-type: application/json');
    echo json_encode('Error [4]: '.$e);
}
