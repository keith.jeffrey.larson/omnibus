<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 10.07.2018
 * Time: 21:33
 */

use Omnibus\Comment;

session_name('__Secure-PHPSESSID');
session_set_cookie_params(
    0,
    '/',
    'sfnw.online',
    true,
    true
);
session_start();

require dirname(__DIR__).'/models/Comment.php';

//Receive the RAW post data.
$content = trim(file_get_contents('php://input'));
//Attempt to decode the incoming RAW post data from JSON.
$data = json_decode($content, true);

if ($_SESSION['fetch_token'] === $data['fetch_token']) {

    Comment::Report($data['comment'], $_SESSION['userid']);
    $msg = $data;

} else {
    header('HTTP/2.0 500 Forbidden');
    die (json_encode(array(
        $_SESSION['fetch_token'],
        $data['fetch_token'],
        $_SESSION
    )));
}

header('HTTP/2.0 200 OK');
header('Content-Type: application/json');
echo json_encode(array(
    'msg' => $msg
));
die();
