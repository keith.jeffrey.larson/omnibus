<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 02.10.2018
 * Time: 01:23
 */

require dirname(__DIR__,4).'/includes/friendlify.php';

$id = $id ?? $_POST['id'] ?? null;

$message = null;
if (isset($_POST['token']) && !empty($_POST['token'])) {
    // Check CSRF
    if ($_POST['token'] === $_SESSION['token']) {
        if (!empty($_POST['id']) && isset($_POST['id'])) {
            $dbg = Article::Edit(
                $id,
                $_POST['title'],
                friendlify($_POST['title']),
                $_POST['body'],
                $_POST['date'],
                $_POST['excerpt'],
                $_POST['category'],
                $_POST['tags'],
                $_POST['author']
            );
        } else {
            $dbg = Article::Add(
                $_POST['title'],
                friendlify($_POST['title']),
                $_POST['body'],
                $_POST['date'],
                $_POST['excerpt'],
                $_POST['category'],
                $_POST['tags'],
                $_POST['author']
            );
        }
    } else {
        die('Unauthorized access');
    }
}

// Load up Twig stuff
require dirname(__DIR__, 4).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$token = Token::Get(64);
$_SESSION['token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('/admin/editors/article.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null,
        'discord_users' => Discord::Online(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),

        'users'         => User::GetAllByRole(true, true, true),
        'article'       => $id === null || $id === '' ? null : Article::GetByID($id),
        'tag_names'     => $id === null || $id === '' ? null : array_column(Tag::GetByArticle($id), 'name'),
        'token'         => $token,
        'message'       => $message,
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}
