<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 31.07.2018
 * Time: 05:27
 */

require_once dirname(__DIR__, 1) . '/backend/models/controller-manager.php';


/**
 * @param $art
 * @param $cat
 * @param $tags
 * @param $auth
 * @return array
 */
function assemble_article($art, $auth) {
    $data = array(
        'id'       => $art->id,
        'title'    => $art->title,
        'furl'     => $art->furl,
        'body'     => $art->body,
        'date'     => $art->date,
        'excerpt'  => $art->excerpt,
        'category' => $art->category,
        'tags'     => $art->tags,
        'author'   => $auth,
    );
    return $data;
}


/**
 * @param $auth
 * @return array
 */
function assemble_author($auth) {
    $data = array(
        'id'     => $auth->id,
        'name'   => $auth->name,
        'title'  => $auth->title,
        'avatar' => $auth->avatar,
        'role'   => $auth->role->name,
    );
    return $data;
}

if (!empty($_GET['id'])) {

    $article = Article::GetByID($_GET['id']);
    $category = $article->category;
    $tags = $article->tags;
    $author = assemble_author($article->author);

    header('Content-Type: application/json');
    echo json_encode(assemble_article($article, $author));

} else if (!empty($_GET['search'])) {

    $articles = isset($_GET['limit']) ? Article::Search($_GET['search'], (int)$_GET['limit']) : Article::Search($_GET['search']);

    header('Content-Type: application/json');

    /** TODO: Pick one way of delivering the API only! */
    if (isset($_GET['dirty'])) {
        echo json_encode($articles);
    } else {
        echo json_encode(array('results' => $articles));
    }

} else {
    $limit = empty($_GET['limit']) ? 0 : $_GET['limit'];

    if (!empty($_GET['category'])) {
        $_articles = Article::GetByCategory($_GET['category'], $limit);
    } else if (!empty($_GET['tag'])) {
        $_articles = Article::GetByTag($_GET['tag'], $limit);
    } else if (!empty($_GET['author'])) {
        $_articles = Article::GetByAuthor($_GET['author'], $limit);
    } else {
        $_articles = Article::GetAll($limit);
    }

    $articles = array();

    foreach ($_articles as $article) {
        $category   = $article->category;
        $tags       = $article->tags;
        $author     = assemble_author   ($article->author);
        $articles[] = assemble_article  ($article, $author);
    }

    header('Content-Type: application/json');
    echo json_encode($articles);
}
