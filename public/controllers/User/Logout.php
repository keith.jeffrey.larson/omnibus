<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 29.09.2018
 * Time: 01:48
 */

// Unset "remember me" cookie
if (isset($_COOKIE['__Secure-rememberme'])) {
    unset($_COOKIE['__Secure-rememberme']);
    setcookie('__Secure-rememberme', '', time()-3600, '/', 'sfnw.online', true, true);
}

// Unset session cookie
if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time()-42000, '/');
}

// Unset all session variables
foreach ($_SESSION as $s) {
    unset($s);
}

// Unset session
unset($_SESSION);

// Destroy session
session_destroy();

// Redirect
header('Location: ' . Settings::Read('domain').'?msg=logout');
die();
