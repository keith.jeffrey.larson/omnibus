<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 13.07.2018
 * Time: 19:54
 */

//require 'vendor/autoload.php';
//require 'backend/models/Database.php';
//require 'backend/models/Article.php';
//require 'backend/models/User.php';
require 'includes/simple-response.php';
//$dbh = Database::Get();
//
//require 'includes/generateToken.php';

//echo PHP_VERSION . '<hr>';

// Title friendlyfying function
//function friendlify($str) {
//    $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
//    $str = trim($str, "-");
//    $str = iconv("utf-8", "us-ascii//TRANSLIT", $str);
//    $str = strtolower($str);
//    $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
//    return $str;
//}

//Get gravatar
//function get_gravatar($email, $s = 80, $d = 'mp', $r = 'g', $img = false, array $atts = array())
//{
//    $url = 'https://www.gravatar.com/avatar/';
//    $url .= md5(strtolower(trim($email)));
//    $url .= "?s=$s&d=$d&r=$r";
//    if ($img) {
//        $url = '<img src="' . $url . '"';
//        foreach ($atts as $key => $val)
//            $url .= ' ' . $key . '="' . $val . '"';
//        $url .= ' />';
//    }
//    return $url;
//}

/**********************************
 * ADD COMMENT THREADS TO ARTICLES
 **********************************/
//    $sql = 'SELECT * FROM `articles`';
//    $sth = $dbh->prepare($sql);
//    $sth->execute();
//
//    $arts = $sth->fetchAll();
//
//    foreach ($arts as $art) {
//        $sql = "INSERT INTO `comment_threads` (`ID_thread`, `source_thread`) VALUES (NULL, 'article')";
//        $sth = $dbh->prepare($sql);
//        $sth->execute();
//
//        $id = $dbh->lastInsertId();
//
//        $sql = "UPDATE `articles` SET `article_ID_thread` = :id WHERE `articles`.`ID_article` = :art";
//        $sth = $dbh->prepare($sql);
//        $sth->execute(
//            array(
//                ':id' => $id,
//                ':art' => $art['ID_article'],
//            )
//        );
//    }

/**********************************
 * ADD GRAVATARS TO USERS
 **********************************/
//$sql = 'SELECT * FROM `users`';
//$sth = $dbh->query($sql);
//
//$users = $sth->fetchAll();
//
//foreach ($users as $user) {
//    echo(get_gravatar($user['email_user'], 200) . ' for user ' . $user['ID_user'] . '<br>');
//}


/***********************************
 * ADD FURLS TO LECTURES
 ***********************************/
//$sql = "SELECT * FROM `articles`";
//$sth = $dbh->prepare($sql);
//try{
//    $sth->execute();
//} catch (PDOException $e) {
//    echo $e;
//}
//$arts = $sth->fetchAll();
//
//foreach ($arts as $art) {
//    $sql = "UPDATE `articles` SET `friendly_title_article` = :furl WHERE `articles`.`ID_article` = :art";
//    $sth = $dbh->prepare($sql);
//
//    $friendly = friendlify($art['title_article']);
//
//    try{
//        $sth->execute(
//            array(
//                ':art' => $art['ID_article'],
//                ':furl' => $friendly
//            )
//        );
//    } catch (PDOException $e) {
//        echo $e;
//    }
//}

/***********************************
 * HTTP to HTTPS
 ***********************************/
//$sql = "SELECT * FROM `articles`";
//$sth = $dbh->prepare($sql);
//try{
//    $sth->execute();
//} catch (PDOException $e) {
//    echo $e;
//}
//$arts = $sth->fetchAll();
//
//foreach ($arts as $art) {
//    $sql = "UPDATE `articles` SET `body_article` = :body WHERE `articles`.`ID_article` = :art";
//    $sth = $dbh->prepare($sql);
//
//    $body = str_replace('http', 'https', $art['body_article']);
//
//    try{
//        $sth->execute(
//            array(
//                ':art' => $art['ID_article'],
//                ':body' => $body
//            )
//        );
//    } catch (PDOException $e) {
//        echo $e;
//    }
//}

// Load articles from Json, add to database

// Title friendlyfying function
//function friendlify($str) {
//    $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
//    $str = trim($str, "-");
//    $str = iconv("utf-8", "us-ascii//TRANSLIT", $str);
//    $str = strtolower($str);
//    $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
//    return $str;
//}
//
//echo date('r', strtotime('Aug 23rd, 2012')) . '<br><br>';
//
//$threads = json_decode($_POST['json']);
//
//$arts = Article::GetAll();
//
//function find($arr, $title){
//    foreach($arr as $struct) {
//        if ($title == $struct->Title) {
//            return $struct;
//            break;
//        }
//    }
//    return null;
//}
//
//foreach ($arts as $a) {
//    echo $a->date . '<br>';
//    if ($a->date == '1970-01-01 01:01:00') {
//        $j = find($threads, $a->title);
//
//        $sql = 'UPDATE `articles`
//                SET `date_article` = :date
//                WHERE `ID_article` = :id';
//        $sth = $dbh->prepare($sql);
//
//        $date = date('Y-m-d', strtotime($j->Date));
//
//        $sth->bindParam(':date', $date);
//        $sth->bindParam(':id', $a->id);
//
//        try {
//            $sth->execute();
//        } catch (PDOException $e) {
//            echo 'Error: ' . $e;
//        }
//
//        echo 'Update from 0000-00-00 00:00:00 to ' . date('Y-m-d H:m:s', strtotime($j->Date)) . ' in ' . $a->title;
//        echo '<br>';
//    }
//}

//
//if (!empty($_POST)) {
//    $threads = json_decode($_POST['json']);
//
//    foreach ($threads as $t) {
//        echo $t->Title . '<br>';
//
//
//        // Create a comment thread
//        $sql = "INSERT INTO `comment_threads` (`ID_thread`, `source_thread`) VALUES (NULL, 'article')";
//        $sth = $dbh->prepare($sql);
//        try {
//            $sth->execute();
//        } catch (PDOException $e) {
//            echo ('Error [0]: ' . $e);
//        }
//
//        $thread_id = $dbh->lastInsertId();
//
//        // If all's cool, add the art to the db
//        $sql = 'INSERT INTO `articles` (title_article, friendly_title_article, body_article, date_article, excerpt_article, article_ID_category, article_ID_user, article_ID_thread)
//                            VALUES (:title, :friendly, :body, :date, :excerpt, :category, :author, :thread)';
//
//        $sth = $dbh->prepare($sql);
//
//        // Friendlify title
//        $friendly = friendlify($t->Title);
//
//        // Make excerpt
//        $excerpt = implode(' ', array_slice(explode(' ', strip_tags($t->Body)), 0, 15));
//        $_excerpt = '>> Author: ' . $t->Author . '\n\r' . $excerpt;
//
//        // Make date
//        $date = date('r', strtotime($t->Date));
//
//        // Constants
//        $cat = 1;
//        $author = 2;
//
//        $sth->bindParam(':title', $t->Title);
//        $sth->bindParam(':friendly', $friendly);
//        $sth->bindParam(':body', $t->Body);
//        $sth->bindParam(':date', $date);
//        $sth->bindParam(':excerpt', $_excerpt);
//        $sth->bindParam(':category', $cat);
//        $sth->bindParam(':author', $author);
//        $sth->bindParam(':thread', $thread_id);
//
//        try {
//            $sth->execute();
//        } catch (PDOException $e) {
//            echo ('Error [1]: ' . $e);
//        }
//
//        $lastid = $dbh->lastInsertId();
//
//        // Add tags too!
//        $tag = 1;
//        $sth = $dbh->prepare('INSERT INTO `article_has_tags` (tags_ID_tag, tags_ID_article)
//                                            VALUES (:tag, :article)');
//
//        $sth->bindParam(':tag', $tag);
//        $sth->bindParam(':article', $lastid);
//
//        try {
//            $sth->execute();
//        } catch (PDOException $e) {
//            echo ('Error [2]: ' . $e);
//        }
//
//        echo ($t->Title . ' added!');
//    }
//}

//$arts = json_decode(json_encode(Article::GetAll()), true);
//
//$fuse = new \Fuse\Fuse($arts,
//    [
//        "keys" => [ "title", "author" ],
//        "includeScore" => true,
//    ]
//);
//
//$r = $fuse->search('write');
//
//foreach (array_chunk($r,5)[0] as $rs) {
//    echo $rs['score'] . @'  >  ' . $rs['item']['title'] .  '<br>';
//}

//foreach (Article::Search('write', 7) as $rs) {
//    echo $rs->author . @'  >  ' . $rs->title .  '<br>';
//}

//if (!empty($_POST)) {
//    $threads = json_decode($_POST['json']);
//
//    foreach ($threads as $t) {
//        $u = User::GetByName($t->Author);
//
//        if ($u === null || !isset($u) || empty($u) || $u === '') {
//            $email = $t->Author . '@tmp.com';
//            $avatar = get_gravatar($email);
//            $role = 2;
//
//            // If everything's fine, add this shit to the database
//            $sql = 'INSERT INTO `users` (name_user, password, email_user, user_ID_role, avatar_user, code_user)
//                            VALUES (:name, :password, :email, :role, :avatar, :code)';
//
//            $sth = $dbh->prepare($sql);
//
//            // Encrypt the password tho
//            $pass = password_hash(getToken(32), PASSWORD_BCRYPT);
//
//            $code = substr(md5(mt_rand()), 0, 15);
//
//            $sth->bindParam(':name', $t->Author);
//            $sth->bindParam(':password', $pass);
//            $sth->bindParam(':email', $email);
//            $sth->bindParam(':role', $role);
//            $sth->bindParam(':avatar', $avatar);
//            $sth->bindParam(':code', $code);
//
//            try {
//                $sth->execute();
//            } catch (PDOException $e) {
//                echo $e . '<br>';
//            }
//            echo $t->Author . ' added!<br>';
//        }
//    }
//}
//
//$users = User::GetAll();
//
//foreach ($users as $user) {
//    $arts = Article::GetBy(Sources::Author, $user->id);
//
//    if (count($arts) < 1) {
//        $sth = $dbh->prepare('UPDATE `users`
//                                        SET `user_ID_role` = :role
//                                        WHERE `ID_user` = :id');
//
//        $role = 3;
//
//        $sth->bindParam(':role', $role);
//        $sth->bindParam(':id', $user->id);
//
//        try {
//            $sth->execute();
//        } catch (PDOException $e) {
//            echo $e;
//        }
//
//        echo $user->name . ' has been demoted<br>';
//    }
//}

?>


