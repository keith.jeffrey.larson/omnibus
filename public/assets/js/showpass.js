function toggle_password(target){
    const d = document;
    const tag = d.getElementById(target);
    const tag2 = d.getElementById("showhide");

    if (tag.getAttribute('type') === 'password'){//tag2.innerHTML === '<i class="grey eye icon"></i>'){
        tag.setAttribute('type', 'text');
        tag2.innerHTML = '<i class="eye icon"></i>';

    } else {
        tag.setAttribute('type', 'password');
        tag2.innerHTML = '<i class="grey eye icon"></i>';
    }
}
