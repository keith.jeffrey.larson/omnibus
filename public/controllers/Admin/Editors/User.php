<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 05.10.2018
 * Time: 23:21
 */

require dirname(__DIR__,4).'/includes/friendlify.php';

$id = $id ?? $_POST['id'] ?? null;

$message = null;
if (isset($_POST['token']) && !empty($_POST['token'])) {
    // Check CSRF
    if ($_POST['token'] === $_SESSION['token']) {
        if (!empty($_POST['id']) && isset($_POST['id'])) {
            $dbg = User::Edit(
                $id,
                $_POST['login'],
                $_POST['bio'],
                $_POST['title'],
                $_POST['link'],
                $_POST['avatar'],
                $_POST['email'],
                $_POST['pass'],
                $_POST['role']
            );
        } else {
            $dbg = User::Add(
                $_POST['login'],
                $_POST['email'],
                $_POST['pass'],
                substr(md5(mt_rand()), 0, 15)
            );
        }
    } else {
        die('Unauthorized access');
    }
}

// Load up Twig stuff
require dirname(__DIR__, 4).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$token = Token::Get(64);
$_SESSION['token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('/admin/editors/user.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => User::GetByID($_SESSION['userid']),
        'discord_users' => Discord::Online(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),

        'e_user'        => $id === null || $id === '' ? null : User::GetByID($id),
        'roles'         => Role::GetLower(User::GetByID($_SESSION['userid'])->role->rank),
        'token'         => $token,
        'message'       => $message,
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}
