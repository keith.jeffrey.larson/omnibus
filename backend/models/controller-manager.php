<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 04:39
 */

require_once 'Article.php';
require_once 'Category.php';
require_once 'Classroom.php';
require_once 'Comment.php';
require_once 'Role.php';
require_once 'Shelf.php';
require_once 'Tag.php';
require_once 'User.php';
require_once 'Quote.php';
require_once 'Discord.php';
require_once 'Uptime.php';
