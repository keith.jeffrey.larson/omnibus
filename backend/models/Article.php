<?php
/**
 * Contains the Article object definition and methods
 *
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 04:20
 */

use Fuse\Fuse;

require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

// GET DATABASE
require_once __DIR__ . '/Database.php';
require_once __DIR__ . '/Sources.php';
require_once __DIR__ . '/Category.php';
require_once __DIR__ . '/Tag.php';
require_once __DIR__ . '/User.php';


class Article
{
    public $id;
    public $title;
    public $furl;
    public $body;
    public $date;
    public $excerpt;
    public $category;
    public $tags;
    public $author;
    public $comments;

    /**
     * Article constructor.
     * @param $id
     * @param $title
     * @param $furl
     * @param $body
     * @param $date
     * @param $excerpt
     * @param $category
     * @param $tags
     * @param $author
     * @param $comments
     */
    public function __construct($id, $title, $furl, $body, $date, $excerpt, $category, $tags, $author, $comments)
    {
        $this->id = $id;
        $this->title = $title;
        $this->furl = $furl;
        $this->body = $body;
        $this->date = $date;
        $this->excerpt = $excerpt;
        $this->category = $category;
        $this->tags = $tags;
        $this->author = $author;
        $this->comments = $comments;
    }

    /**
     * Gets articles with titles that match the query
     * @param string $query Takes a query to match the results against
     * @param int $limit Takes a number of results to return
     * @return array Returns an array of results, ordered by how well they match
     */
    public static function Search(string $query, int $limit = 5): array
    {
        $arts = json_decode(json_encode(self::GetAll()), true);

        $fuse = new Fuse($arts,
            [
                'keys' => ['title', 'author'],
                'includeScore' => true,
            ]
        );

        $r = $fuse->search($query);
        $rl = $limit > 0 ? array_chunk($r, $limit)[0] : $r;

        $out = [];
        foreach ($rl as $art) {
            $out[] = array(
                'id'       => $art['item']['id'],
                'title'    => $art['item']['title'],
                'furl'     => 'https://sfnw.online/a/'.$art['item']['id'].'/'.$art['item']['furl'],
                'body'     => $art['item']['body'],
                'date'     => $art['item']['date'],
                'excerpt'  => $art['item']['excerpt'],
                'category' => $art['item']['category'],
                'tags'     => $art['item']['tags'],
                'author'   => array(
                    'id'       => $art['item']['author']['id'],
                    'name'     => $art['item']['author']['name'],
                    'title'    => $art['item']['author']['title'],
                    'avatar'   => $art['item']['author']['avatar'],
                ),
                'similarity'=> $art['score']
            );
        }

        return $out;
    }


    /**
     * Gets article by the specified ID number
     * @param int $id Takes an Integer that is the desired article ID
     * @return Article|string Returns an Article, or a String with an error
     */
    public static function GetByID(int $id = 1)
    {
        $dbh = Database::Get();
        $sql = 'SELECT `articles`.*, `categories`.*, `users`.*, `comment_threads`.*
            FROM `articles`
                JOIN `categories` ON `articles`.`article_ID_category` = `categories`.`ID_category`
                JOIN `users` ON `articles`.`article_ID_user` = `users`.`ID_user`
                LEFT JOIN `comment_threads` ON `articles`.`article_ID_thread` = `comment_threads`.`ID_thread`
            WHERE `ID_article` = :article';

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':article', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }

        $article = $sth->fetch(PDO::FETCH_ASSOC);

        return self::Build($article);
    }

    /**
     * Gets an article by a specified parameter
     * @param string $by Takes a Source of the articles
     * @param int $id Takes an ID of the selected source
     * @param int $limit Takes a limit of how many results should be returned
     * @param int $offset
     * @return array|string Returns an array of articles or an error string
     */
    private static function Get(string $by='all', int $id = 1, int $limit = 0, int $offset = 0)
    {
        $dbh = Database::Get();
        $sql = 'SELECT `articles`.*, `categories`.*, `tags`.*, `article_has_tags`.*, `users`.*
                FROM `articles`
                    JOIN `categories` ON `articles`.`article_ID_category` = `categories`.`ID_category`
                    JOIN `users` ON `articles`.`article_ID_user` = `users`.`ID_user`
                    JOIN `article_has_tags` ON `article_has_tags`.`tags_ID_article` = `articles`.`ID_article`
                    JOIN `tags` ON `article_has_tags`.`tags_ID_tag` = `tags`.`ID_tag`';
        switch ($by) {
            case 'category':
                $sql .= 'WHERE `ID_category` = :id';
                break;

            case 'author':
                $sql .= 'WHERE `article_ID_user` = :id';
                break;

            case 'tag':
                $sql .= 'WHERE `ID_tag` = :id';
                break;

            default:
                break;
        }
        $sql .= ' ORDER BY `date_article` DESC';

        // Add limit if necessary
        $sql = $limit > 0 ? $sql . ' LIMIT :l' : $sql;
        $li = (int)trim($limit);

        // Add offset if necessary
        $sql = $offset > 0 ? $sql . ' OFFSET :o' : $sql;
        $of = (int)trim($offset);

        $sth = $dbh->prepare($sql);

        if ($limit > 0) {
            $sth->bindParam('l', $li, PDO::PARAM_INT);
        }
        if ($offset > 0) {
            $sth->bindParam('o', $of, PDO::PARAM_INT);
        }

        if ($id !== 0) {
            $sth->bindParam('id', $id, PDO::PARAM_INT);
        }

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            //return 'Error!: [4]' . $e->getMessage() . '<br/>';
        }

        $articles = $sth->fetchAll();

        foreach ($articles as $key => $art) {
            $articles[$key] = self::Build($art);
        }

        return $articles;
    }

    public static function GetAll(int $limit = 0, int $offset = 0): array
    {
        return self::Get('all', 0, $limit, $offset);
    }
    public static function GetByCategory(int $id = 1, int $limit = 0, int $offset = 0): array
    {
        return self::Get('category', $id, $limit, $offset);
    }
    public static function GetByAuthor(int $id = 1, int $limit = 0, int $offset = 0): array
    {
        return self::Get('author', $id, $limit, $offset);
    }
    public static function GetByTag(int $id = 1, int $limit = 0, int $offset = 0): array
    {
        return self::Get('tag', $id, $limit, $offset);
    }

    /**
     * Counts articles currently in the database
     * @return int|string Returns an Integer equal to the amount of articles, or a String with an error
     */
    public static function Count()
    {
        $dbh = Database::Get();
        $sql = 'SELECT COUNT(*)
                FROM `articles`';

        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }

        return (int)$sth->fetchColumn();
    }

    /**
     * @param $title
     * @param $furl
     * @param $body
     * @param $date
     * @param $excerpt
     * @param $category
     * @param $tags
     * @param $author
     * @return null|string
     */
    public static function Add($title, $furl, $body, $date, $excerpt, $category, $tags, $author): ?string
    {
        $dbh = Database::Get();

        // Create a comment thread
        $sql = "INSERT INTO `comment_threads` (`ID_thread`, `source_thread`) VALUES (NULL, 'article')";
        $sth = $dbh->prepare($sql);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        $thread_id = $dbh->lastInsertId();

        // If all's cool, add the art to the db
        $sql = 'INSERT INTO `articles` (title_article, friendly_title_article, body_article, date_article, excerpt_article, article_ID_category, article_ID_user, article_ID_thread)
                            VALUES (:title, :friendly, :body, :date, :excerpt, :category, :author, :thread)';

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':title', $title);
        $sth->bindParam(':friendly', $furl);
        $sth->bindParam(':body', $body);
        $sth->bindParam(':date', $date);
        $sth->bindParam(':excerpt', $excerpt);
        $sth->bindParam(':category', $category);
        $sth->bindParam(':author', $author);
        $sth->bindParam(':thread', $thread_id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        $lastid = $dbh->lastInsertId();

        // Add tags too!
        foreach ($tags as $tag) {
            $sth = $dbh->prepare('INSERT INTO `article_has_tags` (tags_ID_tag, tags_ID_article)
                                                VALUES (:tag, :article)');

            $sth->bindParam(':tag', $tag);
            $sth->bindParam(':article', $lastid);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                return $e->getMessage();
            }
        }
        return (int)$lastid;
    }

    /**
     * Deletes article, its comments, comment thread and tag associations
     * @param int $id ID of the article to be deleted
     * @return string Returns null if successful or an error string
     */
    public static function Delete(int $id): ?string
    {
        $dbh = Database::Get();

        $art = self::GetByID($id);

        // Delete comment
        $sql = 'DELETE FROM `comments` WHERE `comment_ID_thread` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $art->comments);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        // Delete thread
        $sql = 'DELETE FROM `comment_threads` WHERE `ID_thread` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $art->comments);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        // Delete tag associations
        $sql = 'DELETE FROM `article_has_tags` WHERE `tags_ID_article` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        // Delete article itself
        $sql = 'DELETE FROM `articles` WHERE `ID_article` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        return null;
    }

    /**
     * @param $id
     * @param $title
     * @param $furl
     * @param $body
     * @param $date
     * @param $excerpt
     * @param $category
     * @param $tags
     * @param $author
     * @return null|string
     */
    public static function Edit($id, $title, $furl, $body, $date, $excerpt, $category, $tags, $author): ?string
    {
        $dbh = Database::Get();
        // If all's cool, add to the db
        $sth = $dbh->prepare('UPDATE `articles` 
                                            SET `title_article` = :title,
                                                `friendly_title_article` = :friendly,
                                                `body_article` = :body,
                                                `date_article` = :date,
                                                `excerpt_article` = :excerpt,
                                                `article_ID_category` = :category,
                                                `article_ID_user` = :author
                                            WHERE `ID_article` = :id');

        $sth->bindParam(':title', $title);
        $sth->bindParam(':friendly', $furl);
        $sth->bindParam(':body', $body);
        $sth->bindParam(':date', $date);
        $sth->bindParam(':excerpt', $excerpt);
        $sth->bindParam(':category', $category);
        $sth->bindParam(':author', $author);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        // Remove tags
        $sth = $dbh->prepare('DELETE FROM `article_has_tags` 
                                            WHERE `tags_ID_article` = :article');

        $sth->bindParam(':article', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        // Add tags too!
        foreach ($tags as $tag) {
            $sth = $dbh->prepare('INSERT INTO `article_has_tags` (`tags_ID_tag`, `tags_ID_article`) VALUES (:tag, :article)');

            $sth->bindParam(':tag', $tag);
            $sth->bindParam(':article', $id);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                return $e->getMessage();
            }
        }

        return null;
    }

    /**
     * @param $id
     * @param $category
     * @param $tags
     * @param $author
     * @return null|string
     */
    public static function EditDetails($id, $category, $tags, $author): ?string
    {
        $dbh = Database::Get();
        // If all's cool, add to the db
        $sth = $dbh->prepare('UPDATE `articles` 
                                        SET `article_ID_category` = :category,
                                            `article_ID_user` = :author
                                        WHERE `ID_article` = :id');

        $sth->bindParam(':category', $category);
        $sth->bindParam(':author', $author);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        // Remove tags
        $sth = $dbh->prepare('DELETE FROM `article_has_tags` 
                                            WHERE `tags_ID_article` = :article');

        $sth->bindParam(':article', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        // Add tags too!
        foreach ($tags as $tag) {
            $sth = $dbh->prepare('INSERT INTO `article_has_tags` (`tags_ID_tag`, `tags_ID_article`) VALUES (:tag, :article)');

            $sth->bindParam(':tag', $tag);
            $sth->bindParam(':article', $id);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                return $e->getMessage();
            }
        }

        return null;
    }

    /**
     * @param array[Article] $articles
     * @return array
     */
    public static function ParseAll(array $articles): array
    {
        $out = array();
        foreach ($articles as $article) {
            $out[] = $article->Parse();
        }
        return $out;
    }

    /**
     * @return Article
     */
    public function Parse(): Article
    {
        $Parsedown = new Parsedown();
        return new Article(
            $this->id,
            $this->title,
            $this->furl,
            $this->body,
            $this->date,
            $Parsedown->line($this->excerpt),
            $this->category,
            $this->tags,
            $this->author,
            $this->comments
        );
    }

    /**
     * Transforms an assoc array returned from database query into an Article object
     * @param array $article Takes an assoc array describing an Article
     * @return Article Returns a built Article object
     */
    private static function Build($article): Article
    {
        $art = new Article(
            $article['ID_article'],
            $article['title_article'],
            $article['friendly_title_article'],
            $article['body_article'],
            $article['date_article'],
            $article['excerpt_article'],
            Category::GetByID($article['article_ID_category']),
            Tag::GetByArticle($article['ID_article']),
            User::GetByID($article['article_ID_user']),
            $article['article_ID_thread']
        );

        return $art;
    }
}
