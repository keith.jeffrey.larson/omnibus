<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 29.09.2018
 * Time: 01:24
 */

if(!empty($_SESSION['userid'])) {
    header('Location: /');
    die();
}

// Get URL data for redirect
$domain = Settings::Read('domain');

// LOGIN USER
$error = false;
$message = array();
if (!empty($_POST) && !empty($_SESSION)) {

    // Validate tokens
    if ($_POST['token'] !== $_SESSION['token']) {
        die('Access this form from website only.');
    }

    // Check $_POST keys
    $whitelist = array('login', 'password', 'remember', 'token');
    foreach ($_POST as $key=>$item) {
        if (!in_array($key, $whitelist, false)) {
            die('Please use only the fields in the form.');
        }
    }

    // Get user
    $user = null;
    if (filter_var($_POST['login'], FILTER_VALIDATE_EMAIL)) {
        $user  = User::GetByEmail($_POST['login']);
    } else {
        $user = User::GetByName($_POST['login']);
    }

    // Check if user really exists
    if (($user !== null) && password_verify($_POST['password'], $user->password)) {

        // Check if user activated
        if($user->activated) {

            // Remember user
            if ($_POST['remember'] ?? null === 'on') {
                try {
                    $remember_me = bin2hex(random_bytes(256));
                    User::SetRememberMe($user->id, $remember_me);
                    $cookie = $user->id . ':' . $remember_me;
                    $mac = password_hash($cookie, PASSWORD_BCRYPT);
                    $cookie .= ':' . $mac;
                    setcookie('__Secure-rememberme', $cookie, time()+2592000, '/', 'sfnw.online', true, true );
                } catch (Exception $e) {
                    $error = true;
                    $message = array(
                        'type' => 'error',
                        'header' => 'Error!',
                        'body' => 'Cannot generate token',
                    );
                }
            }

            if (!$error) {
                $_SESSION['userid'] = $user->id;
                header('Location: ' . $domain . '?msg=login');
                die();
            }

        } else {
            $message = array(
                'type'   => 'warning',
                'header' => 'Error!',
                'body'   => 'Your account hasn\'t been activated',
            );
        }

    } else {
        $message = array(
            'type'   => 'error',
            'header' => 'Error!',
            'body'   => 'Incorrect credentials!',
        );
    }
}

// Messages
if (isset($_GET['msg'])) {
    switch($_GET['msg']) {
        case 'activated':
            $message = array(
                'type'   => 'success',
                'header' => 'Nice!',
                'body'   => 'Your account is now active – all that\'s left to do is log in!',
            );
            break;
        case 'restored':
            $message = array(
                'type'   => 'success',
                'header' => 'Password sent!',
                'body'   => 'The login page eagerly awaits your return with a new password.',
            );
            break;
    }
}

// Load up Twig stuff
require dirname(__DIR__, 3).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$token = Token::Get(64);
$_SESSION['token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('/user/login.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null,
        'discord_users' => Discord::Online(),
        'quote'         => Quote::GetRandomQuote(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),

        'token'   => $token,
        'message'  => $message,
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}
