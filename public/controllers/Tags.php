<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 28.09.2018
 * Time: 07:01
 */

// Load up Twig stuff
require dirname(__DIR__, 2).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$token = Token::Get(32);
$_SESSION['fetch_token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('tags.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null,
        'discord_users' => Discord::Online(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::ParseAll(Tag::GetAll()),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),
        'token'         => $token,

        'articles'      => empty($id) ? null : Article::ParseAll(Article::GetByTag($id)),
        'tag'           => empty($id) ? null : Tag::GetByID($id)->Parse(),
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}
