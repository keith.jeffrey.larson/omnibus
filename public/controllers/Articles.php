<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 26.11.2018
 * Time: 22:50
 */

// Load up Twig stuff
require dirname(__DIR__, 2).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$user = !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null;
$articles = $p === 'all' ? Article::ParseAll(Article::GetAll()) : Article::ParseAll(Article::GetAll(20, ($p-1)*20));
$token = Token::Get(32);
$_SESSION['fetch_token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('articles.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => $user,
        'discord_users' => Discord::Online(),
        'quote'         => Quote::GetRandomQuote(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),
        'token'         => $token,

        'articles'      => $articles,
        'page'          => $p,
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
} catch (Exception $e) {
    header('Content-type: application/json');
    echo json_encode('Error [4]: '.$e);
}
