<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 28.09.2018
 * Time: 04:11
 */

// Load up Twig stuff
require dirname(__DIR__, 2).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables to pass
$user = !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null;
$token = Token::Get(32);
$_SESSION['fetch_token'] = $token;
$article = Article::GetByID($id);

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('article.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => $user,
        'discord_users' => Discord::Online(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),
        'token'         => $token,

        'article'       => $article->Parse(),
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}

//echo '<pre>'.var_export($_SESSION, true).'</pre>';
//echo '<pre>'.var_export($token, true).'</pre>';
