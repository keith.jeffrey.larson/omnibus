function doneLoading() {
    $('.ui.form')
        .form({
            fields: {
                login: {
                    identifier: 'login',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter your login'
                        }
                    ]
                },
                pass: {
                    identifier: 'pass',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter or generate a password'
                        }
                    ]
                },
                email: {
                    identifier: 'email',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter your email'
                        },
                        {
                            type: 'email',
                            prompt: 'Please enter valid email'
                        }
                    ]
                },
                privacy: {
                    identifier: 'privacy',
                    rules: [
                        {
                            type: 'checked',
                            prompt: 'You must agree to the privacy policy'
                        }
                    ]
                },
                terms: {
                    identifier: 'terms',
                    rules: [
                        {
                            type: 'checked',
                            prompt: 'You must agree to the terms and conditions'
                        }
                    ]
                }
            }
        })
    ;

    $('#privacy-link').click(function () {
        $('#policy').modal('show');
        return false;
    });

    $('#tos-link').click(function () {
        $('#terms').modal('show');
        return false;
    });
}
