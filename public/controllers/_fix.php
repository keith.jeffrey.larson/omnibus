<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.10.2018
 * Time: 05:16
 */

//function friendlify($str)
//{
//    $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
//    $str = trim($str, "-");
//    $str = iconv("utf-8", "us-ascii//TRANSLIT", $str);
//    $str = strtolower($str);
//    $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
//    return $str;
//}
//
//if (!empty($_POST['json'])) {
//    $j = json_decode($_POST['json'], true);
//
//    foreach ($j as $art) {
//        $does_exist = GetByBody($art['Body']) === 1;
//        $is_desired = strpos($art['Title'], 'Lecture') !== false || strpos($art['Title'], 'Pro') !== false;
//
//        echo $art['Title'] . ($does_exist ? '[1]' : '[0]') . ($is_desired ? '[true]' : '[false]') . '<br>';
//
//    }
//}
//
//if (!empty($_POST)) {
//    $threads = json_decode($_POST['json']);
//
//    foreach ($threads as $t) {
//
//        $does_exist = GetByBody($t->Body) === 1 || GetByTitle($t->Title);
//        $is_desired = strpos($t->Title, 'Lecture') !== false || strpos($t->Title, 'Pro') !== false;
//
//        if ($does_exist && $is_desired) {
//            echo '[DUPLICATE]' . $t->Title . '<br>';
//        } else {
//
//            $dbh = Database::Get();
//
//            echo '[ADDED]' . $t->Title . '<br>';
//
//            // Create a comment thread
//            $sql = "INSERT INTO `comment_threads` (`ID_thread`, `source_thread`) VALUES (NULL, 'article')";
//            $sth = $dbh->prepare($sql);
//            try {
//                $sth->execute();
//            } catch (PDOException $e) {
//                echo('Error [0]: ' . $e);
//            }
//
//            $thread_id = $dbh->lastInsertId();
//
//            // If all's cool, add the art to the db
//            $sql = 'INSERT INTO `articles` (title_article, friendly_title_article, body_article, date_article, excerpt_article, article_ID_category, article_ID_user, article_ID_thread)
//                            VALUES (:title, :friendly, :body, :date, :excerpt, :category, :author, :thread)';
//
//            $sth = $dbh->prepare($sql);
//
//            // Friendlify title
//            $friendly = friendlify($t->Title);
//
//            // Make excerpt
//            $excerpt = implode(' ', array_slice(explode(' ', strip_tags($t->Body)), 0, 15));
//            $_excerpt = '>> Author: ' . $t->Author . '\n\r' . $excerpt;
//
//            // Make date
//            $date = date('r', strtotime($t->Date));
//
//            // Constants
//            $cat = 1;
//            $author = 2;
//
//            $sth->bindParam(':title', $t->Title);
//            $sth->bindParam(':friendly', $friendly);
//            $sth->bindParam(':body', $t->Body);
//            $sth->bindParam(':date', $date);
//            $sth->bindParam(':excerpt', $_excerpt);
//            $sth->bindParam(':category', $cat);
//            $sth->bindParam(':author', $author);
//            $sth->bindParam(':thread', $thread_id);
//
//            try {
//                $sth->execute();
//            } catch (PDOException $e) {
//                echo('Error [1]: ' . $e);
//            }
//
//            $lastid = $dbh->lastInsertId();
//
//            // Add tags too!
//            $tag = 1;
//            $sth = $dbh->prepare('INSERT INTO `article_has_tags` (tags_ID_tag, tags_ID_article)
//                                            VALUES (:tag, :article)');
//
//            $sth->bindParam(':tag', $tag);
//            $sth->bindParam(':article', $lastid);
//
//            try {
//                $sth->execute();
//            } catch (PDOException $e) {
//                echo('Error [2]: ' . $e);
//            }
//
//            echo($t->Title . ' added!');
//        }
//    }
//}
//
//
//function GetByBody(string $body)
//{
//    $sql = 'SELECT COUNT(*) FROM `articles`
//                WHERE `body_article` = :body';
//
//    $dbh = Database::Get();
//    $sth = $dbh->prepare($sql);
//
//    $sth->bindParam('body', $body);
//
//    try {
//        $sth->execute();
//    } catch (PDOException $e) {
//        return 'Error!: [4]' . $e->getMessage() . '<br/>';
//    }
//
//    return $sth->fetchColumn();
//}
//
//
//function GetByTitle(string $body)
//{
//    $sql = 'SELECT COUNT(*) FROM `articles`
//                WHERE `title_article` = :body';
//
//    $dbh = Database::Get();
//    $sth = $dbh->prepare($sql);
//
//    $sth->bindParam('body', $body);
//
//    try {
//        $sth->execute();
//    } catch (PDOException $e) {
//        return 'Error!: [4]' . $e->getMessage() . '<br/>';
//    }
//
//    return $sth->fetchColumn();
//}

//foreach ($arts as $a) {
////    $tags = array();
////    foreach ($a->tags as $t) {
////        $tags[] = $t->id;
////    }
////
////    Article::Edit(
////            $a->id,
////            trim(str_replace('Lecture [Advanced] :', '', str_replace('Lecture/Faux-Pas:', '', $a->title))),
////            $a->furl,
////            $a->body,
////            $a->date,
////            $a->excerpt,
////            $a->category->id,
////            $tags,
////            $a->author->id
////    );
////    echo $a->title . '<br>';
////}

