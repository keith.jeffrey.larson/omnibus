<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 30.09.2018
 * Time: 05:08
 */

// Load up Twig stuff
use Omnibus\Comment;

require dirname(__DIR__, 3).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();


// Set up variables
$counters = array(
    'users'      => User::Count(),
    'comments'   => Comment::Count(),
    'articles'   => Article::Count(),
    'categories' => Category::Count(),
    'tags'       => Tag::Count(),
    'classes'    => Classroom::Count()
);

$uptime = array(
    'bot'     => Uptime::GetUptime('discord-bot'),
    'omnibus' => Uptime::GetUptime('omnibus')
);
$downtime = array(
    'bot'     => Uptime::GetDowntime('discord-bot'),
    'omnibus' => Uptime::GetDowntime('omnibus')
);
$status = array(
    'bot'     => Uptime::GetStatus('discord-bot'),
    'omnibus' => Uptime::GetStatus('omnibus')
);
$logs = array(
    'bot'     => Uptime::GetLogs('discord-bot'),
    'omnibus' => Uptime::GetLogs('omnibus')
);
if (($handle = fopen('cron/log.txt', 'rb')) !== FALSE) {
    $cron_log = array();
    foreach (array_filter(explode("\r\n----\r\n", fread($handle, filesize('cron/log.txt')))) as $event) {
        $e = array_filter(explode("\r\n", $event));
        $cron_log[] = array(
            'requested' => $e[0],
            'generated' => $e[1] ?? 'Processing...'
        );
    }
}

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('/admin/dashboard.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null,
        'discord_users' => Discord::Online(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),

        'counter'       => $counters,

        'downtime'      => $downtime,
        'uptime'        => $uptime,
        'status'        => $status,
        'logs'          => $logs,

        'cron'          => $cron_log,
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}
