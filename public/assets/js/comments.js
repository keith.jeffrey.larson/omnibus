getComments();

let form = document.querySelector('#comment_form');
let container = document.getElementById('display_comment');
let html = '';
let reports = document.querySelectorAll('.report-btn');
let area = document.getElementById('comment');
let counter = document.querySelector('.char-counter');
let maxchars = 1000;
console.log(counter);

area.addEventListener('input', function (e) {
   console.log(area.value.length);
   counter.innerHTML = area.value.length + '/' + maxchars;
});

form.addEventListener('submit', function (e) {
    e.preventDefault();

    postData(`/backend/internal_api/add-comments.php`, {
        body: document.getElementById('comment').value,
        thread: document.getElementById('thread').value,
        fetch_token: document.getElementById('comment-token').value,
        action: 'add'
    })
        .then(function (data) {
            getComments();
        })
        .catch(error => console.error(error));
});

function attachReports() {
    reports = document.querySelectorAll('.report-btn');

    console.log(reports.length);
    for(let i = 0; i < reports.length; i++) {
        reports[i].addEventListener("click", function (e) {
            postData('/backend/internal_api/report-comment.php',{
                fetch_token: document.getElementById('comment-token').value,
                comment: reports[i].dataset.id
            })
                .then(function (data) {
                    getComments();
                })
                .catch(error => console.error(error));
        })
    }

    // console.log('attached all');
}

function getComments() {
    html = '';
    let thread = document.getElementById("thread").value;
    fetch('/backend/internal_api/get-comments.php?thread='+thread, {
        method: "GET", // *GET, POST, PUT, DELETE, etc.
        mode: "same-origin", // no-cors, cors, *same-origin
        cache: "default", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
    })

        .then(function (response) {
            return response.text();
        })
        .then(function (json) {
            container.innerHTML = json;
            attachReports();
        });
}

function postData(url = ``, data = {}) {
    return fetch(url, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "same-origin", // no-cors, cors, *same-origin
        cache: "default", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    })
        .then(response => response.json());
}
