<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 21.10.2018
 * Time: 18:22
 */


// Load up Twig stuff
use Omnibus\Comment;

require dirname(__DIR__, 4).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$user_self = !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null;
$user = isset($id) ? User::GetByID($id) : $user_self;
$token = Token::Get(32);
$_SESSION['fetch_token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('/user/profile/profile.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user_self'     => $user_self,
        'discord_users' => Discord::Online(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),
        'token'         => $token,

        'user'          => isset($id) ? User::GetByID($id) : $user_self,
        'counts'        => array(
            'comments'      => Comment::Count($user->id),
            'bookmarks'     => Shelf::CountBookmarks($user->id),
            'favourites'    => Shelf::CountFavourites($user->id)
        ),
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
} catch (Exception $e) {
    header('Content-type: application/json');
    echo json_encode('Error [4]: '.$e);
}
