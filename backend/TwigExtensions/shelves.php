<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 21.10.2018
 * Time: 00:54
 */

require_once dirname(__DIR__) . '/models/Shelf.php';

/** @var $t_check_bookmark Twig_SimpleFunction */
$t_check_bookmark = new Twig_SimpleFunction('check_bookmark', static function (int $article, $user) {
    if ($user !== null) {
        return Shelf::CheckBookmark($article, $user);
    }

    return false;
});

/** @var $t_check_favourite Twig_SimpleFunction */
$t_check_favourite = new Twig_SimpleFunction('check_favourite', static function (int $article, $user) {
    if ($user !== null) {
        return Shelf::CheckFavourite($article, $user);
    }

    return false;
});
