<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 04:45
 */

// GET DATABASE
require_once __DIR__ . '/Database.php';

class Shelf
{
    public $type;
    public $user;
    public $article;

    /**
     * Shelf constructor.
     * @param $type string
     * @param $user int
     * @param $article int
     */
    public function __construct(string $type, int $user, int $article)
    {
        $this->type = $type;
        $this->user = $user;
        $this->article = $article;
    }

    /**
     * @param string $type
     * @param int $article
     * @param int $user
     * @return string|null
     */
    private static function Add(string $type, int $article, int $user): ?string
    {
        $dbh = Database::Get();

        $sql = '';
        switch ($type) {
            case 'B':
                $sql = 'INSERT INTO `bookmarks` (`bookmark_ID_article`, `bookmark_ID_user`) VALUES (:art, :user)';
                break;
            case 'F':
                $sql = 'INSERT INTO `favourites` (`favourite_ID_article`, `favourite_ID_user`) VALUES (:art, :user)';
                break;
        }

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':art', $article);
        $sth->bindParam(':user', $user);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage() . '<br/>';
        }

        return null;
    }

    /**
     * @param int $article
     * @param int $user
     * @return string
     */
    public static function AddBookmark(int $article, int $user): string
    {
        return self::Add('B', $article, $user);
    }

    /**
     * @param int $article
     * @param int $user
     * @return string
     */
    public static function AddFavourite(int $article, int $user): string
    {
        return self::Add('F', $article, $user);
    }

    /**
     * @param string $type
     * @param int $article
     * @param int $user
     * @return string|null
     */
    private static function Remove(string $type, int $article, int $user): ?string
    {
        $dbh = Database::Get();

        $sql = '';
        switch ($type) {
            case 'B':
                $sql = 'DELETE FROM `bookmarks` WHERE `bookmarks`.`bookmark_ID_user` = :user AND `bookmarks`.`bookmark_ID_article` = :art';
                break;
            case 'F':
                $sql = 'DELETE FROM `favourites` WHERE `favourites`.`favourite_ID_user` = :user AND `favourites`.`favourite_ID_article` = :art';
                break;
        }

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':art', $article);
        $sth->bindParam(':user', $user);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage() . '<br/>';
        }

        return null;
    }

    /**
     * @param int $article
     * @param int $user
     * @return string
     */
    public static function RemoveBookmark(int $article, int $user): string
    {
        return self::Remove('B', $article, $user);
    }

    /**
     * @param int $article
     * @param int $user
     * @return string
     */
    public static function RemoveFavourite(int $article, int $user): string
    {
        return self::Remove('F', $article, $user);
    }

    /**
     * @param string $type
     * @param int $article
     * @param int $user
     * @return bool|string
     */
    private static function Check(string $type, int $article, int $user)
    {
        $dbh = Database::Get();

        $sql = '';
        switch ($type) {
            case 'B':
                $sql = 'SELECT * FROM `bookmarks` WHERE `bookmark_ID_article` = :article AND `bookmark_ID_user` = :user';
                break;
            case 'F':
                $sql = 'SELECT * FROM `favourites` WHERE `favourite_ID_article` = :article AND `favourite_ID_user` = :user';
                break;
        }

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':article', $article);
        $sth->bindParam(':user', $user);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage() . '<br/>';
        }

        $bookmark = $sth->fetchAll();

        return !empty($bookmark);
    }

    /**
     * Check if an article has been added to bookmarks by the user
     * @param int $article
     * @param int $user
     * @return bool|string Returns boolean or an error string
     */
    public static function CheckBookmark(int $article, int $user)
    {
        return self::Check('B', $article, $user);
    }

    /**
     * Check if an article has been added to favourites by the user
     * @param int $article
     * @param int $user
     * @return bool|string Returns boolean or an error string
     */
    public static function CheckFavourite(int $article, int $user)
    {
        return self::Check('F', $article, $user);
    }

    /**
     * @param string $type
     * @param int $user
     * @return array|string
     */
    private static function Get(string $type, int $user)
    {
        $dbh = Database::Get();

        $sql = '';
        switch ($type) {
            case 'B':
                $sql = 'SELECT * FROM `bookmarks` WHERE `bookmark_ID_user` = :user';
                break;
            case 'F':
                $sql = 'SELECT * FROM `favourites` WHERE `favourite_ID_user` = :user';
                break;
        }

        $sth = $dbh->prepare($sql);
        $sth->bindParam(':user', $user);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [9]' . $e->getMessage() . '<br/>';
        }

        $shelves = $sth->fetchAll();

        switch ($type) {
            case 'B':
                foreach ($shelves as $key => $bm) {
                    $shelves[$key] = self::FriendlifyBookmark($bm);
                }
                break;
            case 'F':
                foreach ($shelves as $key => $bm) {
                    $shelves[$key] = self::FriendlifyFavourite($bm);
                }
                break;
        }

        return $shelves;
    }

    /**
     * Gets all articles bookmarked by the user
     * @param int $user
     * @return array|string Returns array of bookmarks or an error string
     */
    public static function GetBookmarks(int $user)
    {
        return self::Get('B', $user);
    }

    /**
     * Gets all articles favourited by the user
     * @param int $user
     * @return array|string Returns array of favourites or an error string
     */
    public static function GetFavourites(int $user)
    {
        return self::Get('F', $user);
    }

    /**
     * @param string $type
     * @param int $user
     * @return mixed|string
     */
    private static function Count(string $type, int $user) {
        $dbh = Database::Get();

        $sql = '';
        switch ($type) {
            case 'B':
                $sql = 'SELECT COUNT(*) FROM `bookmarks` WHERE `bookmark_ID_user` = :user';
                break;
            case 'F':
                $sql = 'SELECT COUNT(*) FROM `favourites` WHERE `favourite_ID_user` = :user';
                break;
        }

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':user', $user);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage() . '<br/>';
        }

        return $sth->fetchColumn();
    }

    /**
     * @param int $user
     * @return mixed|string
     */
    public static function CountBookmarks(int $user) {
        return self::Count('B', $user);
    }

    /**
     * @param int $user
     * @return mixed|string
     */
    public static function CountFavourites(int $user) {
        return self::Count('F', $user);
    }

    /**
     * @param array $bookmark
     * @return Shelf
     */
    private static function FriendlifyBookmark(array $bookmark): Shelf
    {
        return new Shelf('B',
            $bookmark['bookmark_ID_user'],
            $bookmark['bookmark_ID_article']
        );
    }

    /**
     * @param array $favourite
     * @return Shelf
     */
    private static function FriendlifyFavourite(array $favourite): Shelf
    {
        return new Shelf('F',
            $favourite['favourite_ID_user'],
            $favourite['favourite_ID_article']
        );
    }
}
