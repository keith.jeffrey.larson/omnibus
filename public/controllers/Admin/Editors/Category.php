<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 05.10.2018
 * Time: 20:41
 */

require dirname(__DIR__,4).'/includes/friendlify.php';

$id = $id ?? $_POST['id'] ?? null;

$message = null;
if (isset($_POST['token']) && !empty($_POST['token'])) {
    // Check CSRF
    if ($_POST['token'] === $_SESSION['token']) {
        if (!empty($_POST['id']) && isset($_POST['id'])) {
            $dbg = Category::Edit(
                $id,
                $_POST['name'],
                $_POST['description'],
                $_POST['image']
            );
        } else {
            $dbg = Category::Add(
                $_POST['name'],
                $_POST['description'],
                $_POST['image']
            );
        }
    } else {
        die('Unauthorized access');
    }
}

// Load up Twig stuff
require dirname(__DIR__, 4).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$token = Token::Get(64);
$_SESSION['token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('/admin/editors/category.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null,
        'discord_users' => Discord::Online(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),

        'category'      => $id === null || $id === '' ? null : Category::GetByID($id),
        'token'         => $token,
        'message'       => $message,
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}
