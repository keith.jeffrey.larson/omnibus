$('#account')
    .form({
        fields: {
            word: {
                identifier: 'password',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'You need to confirm edits with your current password'
                    }
                ]
            },
            newpass2: {
                identifier: 'newpass2',
                optional: true,
                rules: [
                    {
                        type: 'match[newpass1]',
                        prompt: 'Passwords do not match'
                    }
                ]
            },
            email1: {
                identifier: 'email1',
                optional: true,
                rules: [
                    {
                        type: 'email',
                        prompt: 'Please enter valid email'
                    }
                ]
            }
        }
    })
;
