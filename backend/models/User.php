<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 16:45
 */

use Fuse\Fuse;
use Omnibus\Comment;

require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

// GET DATABASE
require_once __DIR__ . '/Database.php';
require_once __DIR__ . '/Role.php';
require_once dirname(__DIR__) . '/Gravatar.php';

/**
 * Class User
 */
class User
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $bio;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $link;
    /**
     * @var string
     */
    public $avatar;
    /**
     * @var string
     */
    public $email;
    /**
     * @var datetime
     */
    public $joined;
    /**
     * @var datetime
     */
    public $last_seen;
    /**
     * @var Comment
     */
    public $comments;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $code;
    /**
     * @var bool
     */
    public $activated;
    /**
     * @var bool
     */
    public $muted;
    /**
     * @var Role
     */
    public $role;

    /**
     * User constructor.
     * @param $id
     * @param $name
     * @param $bio
     * @param $title
     * @param $link
     * @param $avatar
     * @param $email
     * @param $joined
     * @param $last_seen
     * @param $comments
     * @param $password
     * @param $code
     * @param $activated
     * @param $muted
     * @param $role
     */
    public function __construct($id, $name, $bio, $title, $link, $avatar, $email, $joined, $last_seen, $comments, $password, $code, $activated, $muted, $role)
    {
        $this->id = $id;
        $this->name = $name;
        $this->bio = $bio;
        $this->title = $title;
        $this->link = $link;
        $this->avatar = $avatar;
        $this->email = $email;
        $this->joined = $joined;
        $this->last_seen = $last_seen;
        $this->comments = $comments;
        $this->password = $password;
        $this->code = $code;
        $this->activated = $activated;
        $this->muted = $muted;
        $this->role = $role;
    }

    /**
     * Gets all users that exist in the database
     * @return array|string Returns an array of users or a string with an error
     */
    public static function GetAll()
    {
        $dbh = Database::Get();

        // Fetch all users
        $sql = 'SELECT * FROM `users`
                ORDER BY `user_ID_role`, `name_user`';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [16]' . $e->getMessage() . '<br/>';
        }

        $users = $sth->fetchAll();

        foreach ($users as $key => $user) {
            $users[$key] = $user !== null ? self::Build($user) : null;
        }
        return $users;
    }

    /**
     * Gets all users that possess one of the specified roles
     * @param bool $user Should return users with User role?
     * @param bool $author Should return users with Author role?
     * @param bool $admin Should return users with Admin role?
     * @return array|string Returns an array of User objects or a string with an error
     */
    public static function GetAllByRole(bool $user = false, bool $author = false, bool $admin = false)
    {
        $dbh = Database::Get();

        $roles = array();
        $roles[0] = ($user === true) ? '`user_ID_role` = 3' : '';
        $roles[1] = ($author === true) ? '`user_ID_role` = 2' : '';
        $roles[2] = ($admin === true) ? '`user_ID_role` = 1' : '';
        $roles = implode(' OR ', array_filter($roles, '\strlen'));

        // Fetch all users
        $sql = 'SELECT * FROM `users` WHERE ' . $roles . '
                ORDER BY `user_ID_role`, `name_user`';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [17]' . $e->getMessage() . '<br/>';
        }

        $users = $sth->fetchAll();

        foreach ($users as $key => $u) {
            $users[$key] = $u !== null ? self::Build($u) : null;
        }
        return $users;
    }

    /**
     * Gets user by the specified ID number
     * @param int $id Takes an Integer that is the desired user ID
     * @return User|string Returns a User, or a String with an error
     */
    public static function GetByID(int $id)
    {
        return self::Get('id', $id);
    }

    /**
     * Gets user by name
     * @param string $name Takes a string that is the user name
     * @return User|string Returns a User, or a String with an error
     */
    public static function GetByName(string $name)
    {
        return self::Get('name', $name);
    }

    /**
     * Gets user by email
     * @param string $email Takes a string that is the user email
     * @return User|string Returns a User, or a String with an error
     */
    public static function GetByEmail(string $email)
    {
        return self::Get('email', $email);
    }

    /**
     * Checks if a user with the token exists
     * @param string $token Takes a token to check against
     * @return bool|string Returns true if user has been found, false if not, or a string with an error
     */
    public static function FindByToken(string $token)
    {
        return self::Get('token', $token);
    }

    /**
     * @param string $item
     * @param $id
     * @return string|User|null
     */
    private static function Get(string $item, $id) {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM `users`';

        switch ($item) {
            case 'id': $sql .= ' WHERE `ID_user` = :id';
                break;
            case 'name': $sql .= ' WHERE `name_user` = :id';
                break;
            case 'email': $sql .= ' WHERE `email_user` = :id';
                break;
            case 'token': $sql .= ' WHERE `code_user` = :id';
                break;
        }

        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [18]' . $e->getMessage() . '<br/>';
        }

        $user = $sth->fetch(PDO::FETCH_ASSOC);
        return ($user === null || $user === false) ? null : self::Build($user);
    }

    /**
     * Gets users with titles that match the query
     * @param string $query Takes a query to match the results against
     * @param int $limit Takes a number of results to return
     * @return array Returns an array of results, ordered by how well they match
     */
    public static function Search(string $query, int $limit = 5): array
    {
        $users = json_decode(json_encode(self::GetAll()), true);

        $fuse = new Fuse($users,
            [
                'keys' => ['name'],
                'includeScore' => true,
            ]
        );

        $r = $fuse->search($query);
        $rl = $limit > 0 ? array_chunk($r, $limit)[0] : $r;

        $out = [];
        foreach ($rl as $user) {
            $out[] = array(
                'id'        => $user['item']['id'],
                'name'      => $user['item']['name'],
                'bio'       => $user['item']['bio'],
                'title'     => $user['item']['title'],
                'link'      => $user['item']['link'],
                'avatar'    => $user['item']['avatar'],
                'joined'    => $user['item']['joined'],
                'last seen' => $user['item']['last seen'],
                'role'      => $user['item']['role']['name'],
                'similarity'=> $user['score']
            );
        }

        return $out;
    }

    /**
     * @param string $login
     * @param string $email
     * @param string $password
     * @param string $code
     * @return bool|string
     */
    public static function Add(string $login, string $email, string $password, string $code)
    {
        $dbh = Database::Get();
        $sql = 'INSERT INTO `users` (name_user, password, email_user, user_ID_role, code_user, avatar_user) 
                             VALUES (:name, :password, :email, :role, :code, :avatar)';
        $sth = $dbh->prepare($sql);

        // Encrypt the password tho
        $pass = password_hash($password, PASSWORD_ARGON2I);

        $avatar = Gravatar::Get($email);

        $sth->bindParam(':name', $login);
        $sth->bindParam(':password', $pass);
        $sth->bindParam(':email', $email);
        $sth->bindValue(':role', 3, PDO::PARAM_INT);
        $sth->bindParam(':code', $code);
        $sth->bindParam(':avatar', $avatar);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: ' . $e->getMessage() . '<br/>';
        }

        return true;
    }

    /**
     * @param int $id
     * @param string $login
     * @param string $bio
     * @param string $title
     * @param string $link
     * @param string $avatar
     * @param string $email
     * @param string $password
     * @param int $role
     * @return null|string
     */
    public static function Edit(int $id, string $login, string $bio, string $title, string $link, string $avatar, string $email, string $password, int $role): ?string
    {
        $dbh = Database::Get();

        // Get user
        $sql = 'SELECT `users`.*, `roles`.*
                FROM `users`
                    JOIN `roles` ON `roles`.`ID_role` = `users`.`user_ID_role`
                WHERE `ID_user` = :id
                LIMIT 1';

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        $user = $sth->fetch();

        // Modify user
        $sth = $dbh->prepare('UPDATE `users` 
                                        SET `name_user` = :name, 
                                            `bio_user` = :bio,
                                            `title_user` = :title,
                                            `link_user` = :link,
                                            `avatar_user` = :avatar,
                                            `email_user` = :email,
                                            `password` = :password,  
                                            `user_ID_role` = :role
                                        WHERE `ID_user` = :id');

        // Encrypt pass
        $pass = password_hash($password, PASSWORD_BCRYPT);

        // Check if variables are set
        $login = empty($login) ? $user['name_user'] : $login;
        $sth->bindParam(':name', $login);

        $bio = empty($bio) ? $user['bio_user'] : $bio;
        $sth->bindParam(':bio', $bio);

        $title = empty($title) ? $user['title_user'] : $title;
        $sth->bindParam(':title', $title);

        $link = empty($link) ? $user['link_user'] : $link;
        $sth->bindParam(':link', $link);

        $avatar = empty($avatar) ? $user['avatar_user'] : $avatar;
        $sth->bindParam(':avatar', $avatar);

        $email = empty($email) ? $user['email_user'] : $email;
        $sth->bindParam(':email', $email);

        $pwd = empty($password) ? $user['password'] : $pass;
        $sth->bindParam(':password', $pwd);

        $role = empty($role) ? $user['user_ID_role'] : $role;
        $sth->bindParam(':role', $role);

        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        return null;
    }

    /**
     * Activates the user's account
     * @param string $code
     * @return null|string Returns null or a string with an error
     */
    public static function Activate(string $code): ?string
    {
        $dbh = Database::Get();

        // Modify user
        $sql = 'UPDATE `users` 
            SET `activated_user` = true 
            WHERE `code_user` = :code';

        $sth = $dbh->prepare($sql);

        try {
            $sth->execute(
                array(
                    ':code' => $code,
                )
            );
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }
        return null;
    }

    /**
     * Updates the LastSeen date of the user
     * @param $id int Takes the ID of the user who's status needs to be updated
     * @return null|string Returns null or a string with an error
     */
    public static function UpdateLastSeen(int $id): ?string
    {
        $dbh = Database::Get();
        $date = date('Y-m-d H:i:s');

        // Modify user
        $sql = 'UPDATE `users` 
            SET `last_seen_user` = :date
            WHERE `ID_user` = :id';

        $sth = $dbh->prepare($sql);

        try {
            $sth->execute(
                array(
                    ':date' => $date,
                    ':id' => $id,
                )
            );
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }
        return null;
    }

    /**
     * Deletes the user from database
     * @param int $id ID of the user to be deleted
     * @return null|string Returns null if successful or a n error string
     */
    public static function Delete(int $id): ?string
    {
        $dbh = Database::Get();
        // Change all articles' author to [DELETED]
        $sql = 'UPDATE `articles`
                SET `article_ID_user` = 25
                WHERE `article_ID_user` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        // Change all comments' author to [DELETED]
        $sql = 'UPDATE `comments`
                SET `comment_ID_user` = 25
                WHERE `comment_ID_user` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        // Delete user
        $sql = 'DELETE FROM `users` WHERE `ID_user` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        return null;
    }

    /**
     * Counts users currently in database
     * @return int|string Returns an Integer equal to the amount of users, or a String with an error
     */
    public static function Count()
    {
        $dbh = Database::Get();
        $sql = 'SELECT COUNT(*)
            FROM `users`
            WHERE `activated_user` = TRUE';

        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }

        return (int)$sth->fetchColumn();
    }

    /**
     * @param int $id
     * @param string $token
     * @return int|string
     */
    public static function SetRememberMe(int $id, string $token)
    {
        $dbh = Database::Get();

        // Modify user
        $sql = 'UPDATE `users` 
                SET `remember_me` = :token
                WHERE `ID_user` = :id';

        $sth = $dbh->prepare($sql);

        $new = null;
        try {
            $sth->execute(
                array(
                    ':token' => $token,
                    ':id' => $id,
                )
            );
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }

        return (int)$sth->rowCount();
    }

    /**
     * @param int $id
     * @return mixed|null|string
     */
    public static function GetRememberMe(int $id)
    {
        $dbh = Database::Get();

        $sql = 'SELECT `remember_me` 
                FROM `users` 
                WHERE `ID_user` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [18]' . $e->getMessage() . '<br/>';
        }

        $user = $sth->fetchColumn();
        return ($user === null || $user === false) ? null : $user;
    }

    /**
     * @param string $title
     * @param string $link
     * @param string $path
     * @return null|string
     */
    public function EditProfile(string $title, string $link, ?string $path): ?string
    {
        $dbh = Database::Get();

        $sql = 'UPDATE `users` 
                    SET `title_user` = :title,
                        `link_user` = :link,
                        `avatar_user` = :avatar
                    WHERE `ID_user` = :id';
        $sth = $dbh->prepare($sql);

        // Check if variables are set
        $title = empty($title) ? $this->title : $title;
        $sth->bindParam(':title', $title);

        $link = empty($link) ? $this->link : $link;
        $sth->bindParam(':link', $link);

        $avatar = empty($path) ? $this->avatar : $path;
        $sth->bindParam(':avatar', $avatar);

        $sth->bindParam(':id', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        return null;
    }

    /**
     * @param string $bio
     * @return null|string
     */
    public function EditBio(string $bio): ?string
    {
        $dbh = Database::Get();

        $sql = 'UPDATE `users` 
                    SET `bio_user` = :bio
                    WHERE `ID_user` = :id';
        $sth = $dbh->prepare($sql);

        // Check if variables are set
        $bio = empty($bio) ? $this->bio : $bio;
        $sth->bindParam(':bio', $bio);

        $sth->bindParam(':id', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        return null;
    }

    /**
     * @param string $email
     * @param string $password
     * @return null|string
     */
    public function EditAccount(string $email, string $password): ?string
    {
        $dbh = Database::Get();
        // Modify user
        $sql = 'UPDATE `users` 
                SET `email_user` = :email,
                    `password` = :password
                WHERE `ID_user` = :id';
        $sth = $dbh->prepare($sql);

        // Check if variables are set
        $_email = empty($email) ? $this->email : $email;
        $sth->bindParam(':email', $_email);

        $pass = empty($password) ? $this->password : password_hash($password, PASSWORD_BCRYPT);
        $sth->bindParam(':password', $pass);

        $sth->bindParam(':id', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        return null;
    }

    /**
     * Transforms an assoc array returned from database query into a User object
     * @param array $user Takes an assoc array describing a User
     * @return User returns a built User object
     */
    protected static function Build(array $user): User
    {
        $u = new User(
            $user['ID_user'],
            $user['name_user'],
            $user['bio_user'],
            $user['title_user'],
            $user['link_user'],
            $user['avatar_user'],
            $user['email_user'],
            $user['joined_user'],
            $user['last_seen_user'],
            $user['thread_ID_user'],
            $user['password'],
            $user['code_user'],
            $user['activated_user'],
            $user['muted_user'],
            Role::GetByID($user['user_ID_role'])
        );
        return $u;
    }
}
