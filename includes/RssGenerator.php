<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 04.08.2018
 * Time: 02:13
 */

class RssGenerator
{
    public static function OutputRss(): void
    {
        $items = '';
        foreach (self::MakeRss() as $art) {
            $item = '
                <item>
                    <title>'.$art['title'].'</title>
                    <guid isPermaLink="true">https://sfnw.online/lecture/'.$art['id'].'/'.$art['furl'].'</guid>
                    <dc:creator>'.$art['author']['name'].'</dc:creator>
                    <description>'.$art['excerpt'].'</description>
                    <pubDate>'.date('D, d M Y H:i:s O', strtotime($art['date'])).'</pubDate>
                </item>';
            $items .= $item;
        }

        $rssfeed =
            '<?xml version="1.0" encoding="ISO-8859-1"?>
            <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
                <channel>
                    <title>School for New Writers RSS</title>
                    <link>https://sfnw.online</link>
                    <atom:link href="https://sfnw.online/RSS" rel="self" type="application/rss+xml" />
                    <description>Always keep up to date with new lectures!</description>
                    <language>en-us</language>
                    <copyright>Copyright (C) 2018 sfnw.online</copyright>
                    ' .$items.'
                </channel>
            </rss>';
        echo $rssfeed;
    }

    protected static function MakeRss() {
        $json = file_get_contents('https://sfnw.online/api/articles.php');
        return json_decode($json, TRUE);
    }
}
