function changeTheme(theme, elem) {
    document.cookie = "theme="+theme+"; expires=Thu, 01 Jan 2100 00:00:00 UTC; path=/";

    let semantic, additional;

    switch (theme) {
        case ('dark'):
            semantic = "semantic-dark.min.css";
            additional = "dark.min.css";
            break;
        case ('black'):
            semantic = "semantic-black.min.css";
            additional = "dark.min.css";
            break;
        default:
            semantic = "semantic.min.css";
            additional = "light.min.css";
            break;
    }

    document.querySelector('#semantic-theme').href = '/semantic/dist/'+semantic;
    document.querySelector('#additional-theme').href = '/public/assets/css/'+additional;

    document.querySelector(".check").classList.remove('check');
    elem.querySelector('.icon').classList.add('check');
}
