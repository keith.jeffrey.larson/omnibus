$(document).ready(function () {
    const ttsButton = $('#tts-btn');

    if (!('speechSynthesis' in window)) {
        ttsButton.addClass('collapsed');
    }

    function tts() {
        console.log('TTS');
        if (!('speechSynthesis' in window)) {
            alert("Your browser doesn't support text-to-speech!");
            return;
        }

        const synth = window.speechSynthesis;

        if (synth.paused) {
            synth.resume();
            $('#tts-ico').removeClass('headphones').removeClass('play').addClass('pause')
        } else if (synth.speaking) {
            synth.pause();
            $('#tts-ico').removeClass('headphones').removeClass('pause').addClass('play')
        } else {
            let msg = new SpeechSynthesisUtterance(document.getElementById('art-body').innerText);

            synth.speak(msg);
            $('#tts-ico').removeClass('headphones').addClass('pause')
        }
    }

    ttsButton.on('click', function (event) {
        tts();
    });

    $('#info-btn').on('click', function (event) {
        $('.ui.modal').modal('show')
    })
});

$(':checkbox#dyslexic-toggle').change(function () {
    if ($('#dyslexic-toggle').prop('checked')) {
        $('#content').find('*').addClass('font-dyslexic')
    } else {
        $('#content').find('*').removeClass('font-dyslexic')
    }
});
