let buttons = document.querySelectorAll('.shelf-btn');
let token = document.getElementById('token').dataset.token;

for (let i=0; i<buttons.length; i++) {
    let icon = buttons[i].querySelector("i");

    buttons[i].addEventListener('click', function (e) {
        postData(`/backend/internal_api/shelves.php`, {
            shelf: buttons[i].dataset.shelf,
            article: buttons[i].dataset.article,
            fetch_token: token
        })
            .then(function (data) {
                if (data.item === 'bookmark' && data.msg === 'added') {
                    icon.classList.add('yellow');
                } else if (data.item === 'bookmark' && data.msg === 'removed') {
                    icon.classList.remove('yellow');
                } else if (data.item === 'favourite' && data.msg === 'added') {
                    icon.classList.add('red');
                } else if (data.item === 'favourite' && data.msg === 'removed') {
                    icon.classList.remove('red');
                }
            })
            .catch(error => console.error(error));
    });
}

function postData(url = ``, data = {}) {
    // Default options are marked with *
    return fetch(url, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "same-origin", // no-cors, cors, *same-origin
        cache: "default", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    })
        .then(response => response.json()); // parses response to JSON
}
