const small = function () {
    return window.innerWidth <= 610;
};
const medium = function () {
    return window.innerWidth > 610 && window.innerWidth <= 900;
};
const large = function () {
    return window.innerWidth > 900;
};

function initClasses() {
    let sidebar = document.querySelector("#sidebar");
    let fixedButtons = document.querySelector("#fixed-buttons");
    let social = document.querySelector(".social-widget");
    let comment = document.querySelector(".comment-btn");

    if (small()) {
        if (sidebar !== null) sidebar.classList.remove('desktop').classList.add('mobile');
        if (fixedButtons !== null) fixedButtons.classList.add('mobile');
        if (social !== null) social.classList.add('hidden');
        if (comment !== null) comment.classList.add('full');
    } else if (medium()) {
        if (sidebar !== null) sidebar.classList.remove('desktop').classList.add('mobile');
        if (fixedButtons !== null) fixedButtons.classList.add('mobile');
        if (social !== null) social.classList.add('hidden');
        if (comment !== null) comment.classList.add('full');
    } else {
        if (sidebar !== null) sidebar.classList.remove('mobile').classList.add('desktop');
        if (fixedButtons !== null) fixedButtons.classList.remove('mobile');
        if (social !== null) social.classList.remove('hidden');
        if (comment !== null) comment.classList.remove('full');
    }
}

initClasses();

window.onresize = function(event) {
    initClasses();
};
