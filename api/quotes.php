<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 15.08.2018
 * Time: 05:44
 */

require_once '../backend/models/Quote.php';

$quote = Quote::GetRandomQuote();

header('Content-Type: application/json');
echo json_encode($quote);
