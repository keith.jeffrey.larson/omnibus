<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 16:39
 */

use Fuse\Fuse;

require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

// GET DATABASE
require_once __DIR__ . '/Database.php';

class Category
{
    public $id;
    public $name;
    public $furl;
    public $description;
    public $picture;

    /**
     * Category constructor.
     * @param int $id
     * @param string $name
     * @param string $description
     * @param string $picture
     * @param null $furl
     */
    public function __construct($id, $name, $description, $picture, $furl = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->furl = $furl ?? self::friendlify($name);
        $this->description = $description;
        $this->picture = $picture;
    }

    /**
     * Gets all categories that exist in the database
     * @return array|string Returns an array of categories or a string with an error
     */
    public static function GetAll()
    {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM `categories` ORDER BY `name_category` ASC';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [14]' . $e->getMessage() . '<br/>';
        }

        $categories = $sth->fetchAll();

        foreach ($categories as $key => $cat) {
            $categories[$key] = self::Build($cat);
        }
        return $categories;
    }

    /**
     * Gets category by the specified ID number
     * @param int $id Takes an Integer that is the desired category ID
     * @return Category|string Returns a Category, or a String with an error
     */
    public static function GetByID(int $id = 1)
    {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM `categories` WHERE `ID_category` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [15]' . $e->getMessage() . '<br/>';
        }

        $category = $sth->fetch(PDO::FETCH_ASSOC);
        return self::Build($category);
    }

    /**
     * Gets categories with titles that match the query
     * @param string $query Takes a query to match the results against
     * @param int $limit Takes a number of results to return
     * @return array Returns an array of results, ordered by how well they match
     */
    public static function Search(string $query, int $limit = 5): array
    {
        $cats = json_decode(json_encode(self::GetAll()), true);

        $fuse = new Fuse($cats,
            [
                'keys' => ['name'],
                'includeScore' => true,
            ]
        );

        $r = $fuse->search($query);
        $rl = $limit > 0 ? array_chunk($r, $limit)[0] : $r;

        $out = [];
        foreach ($rl as $category) {
            $out[] = array(
                'id' => $category['item']['id'],
                'name' => $category['item']['name'],
                'description' => $category['item']['description'],
                'picture' => $category['item']['picture'],
                'furl' => 'https://sfnw.online/c/'.$category['item']['id'].'/'.$category['item']['furl'],
                'score' => $category['score']
            );
        }

        return $out;
    }

    /**
     * Deletes a category from the database, provided no articles have it
     * @param int $id ID of the category to be deleted
     * @return null|string Returns null if successful or an error string
     */
    public static function Delete(int $id): ?string
    {
        $dbh = Database::Get();
        // Grab all articles and check if any has this category associated with it
        $sql = 'SELECT * FROM `articles` WHERE `article_ID_category` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        $articles = $sth->fetchAll();

        if (!empty($articles)) {
            return count($articles) . ' articles with this category exist';
        }

        $sql = 'DELETE FROM `categories` WHERE `ID_category` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        return null;
    }

    /** Adds Category to database
     * @param string $name
     * @param string $description
     * @param string $image
     * @return null|string
     */
    public static function Add(string $name, string $description, string $image): ?string
    {
        $dbh = Database::Get();
        // Prepare image placeholder
        $img = empty($image) ? 'https://sfnw.online/public/assets/img/placeholder.png' : $image;

        // If everything's fine, add this shit to the database
        $sql = 'INSERT INTO `categories` (name_category, furl_category, description_category, picture_category) 
                VALUES (:name, :furl, :desc, :picture)';

        $sth = $dbh->prepare($sql);

        $furl = self::friendlify($name);
        $sth->bindParam(':name', $name);
        $sth->bindParam(':furl', $furl);
        $sth->bindParam(':desc', $description);
        $sth->bindParam(':picture', $img);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        return null;
    }

    /**
     * Edits category
     * @param int $id
     * @param string $name
     * @param string $description
     * @param string $image
     * @return null|string
     */
    public static function Edit(int $id, string $name, string $description, string $image): ?string
    {
        $dbh = Database::Get();
        // If everything's fine, add this shit to the database
        $sth = $dbh->prepare('UPDATE `categories` 
                                            SET `name_category` = :name,
                                                `furl_category` = :furl,
                                                `description_category` = :desc,
                                                `picture_category` = :picture
                                            WHERE `ID_category` = :id');

        $furl = self::friendlify($name);
        $sth->bindParam(':name', $name);
        $sth->bindParam(':furl', $furl);
        $sth->bindParam(':desc', $description);
        $sth->bindParam(':picture', $image);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        return null;
    }

    /**
     * Counts categories currently in database
     * @return int|string Returns an Integer equal to the amount of categories, or a String with an error
     */
    public static function Count()
    {
        $dbh = Database::Get();
        $sql = 'SELECT COUNT(*) FROM `categories`';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }

        return (int)$sth->fetchColumn();
    }

    /**
     * Friendlifies the name
     * @param string $str String to be friendlified
     * @return null|string Friendlified string
     */
    private static function friendlify(string $str): ?string
    {
        $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
        $str = trim($str, '-');
        $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
        return $str;
    }

    /**
     * Transforms an assoc array returned from database query into a Category object
     * @param array $category Takes an assoc array describing a Category
     * @return Category returns a built Category object
     */
    private static function Build(array $category): Category
    {
        $cat = new Category(
            $category['ID_category'],
            $category['name_category'],
            $category['description_category'],
            $category['picture_category']
        );

        return $cat;
    }

    /**
     * Gets Category with MD description and parses it into HTML one
     * @return Category
     */
    public function Parse(): Category
    {
        $Parsedown = new Parsedown();
        return new Category(
            $this->id,
            $this->name,
            $Parsedown->text($this->description),
            $this->picture
        );
    }

    /** Parses all categories in an array with Category::Parse()
     * @param array $categories
     * @return array
     */
    public static function ParseAll(array $categories): array
    {
        $out = array();
        foreach ($categories as $category) {
            $out[] = $category->Parse();
        }
        return $out;
    }
}
