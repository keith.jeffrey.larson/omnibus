<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 15.10.2018
 * Time: 05:14:01
 */

$message = null;
if (isset($_POST['token']) && !empty($_POST['token'])) {
    // Check CSRF
    if ($_POST['token'] === $_SESSION['token']) {
        if (!empty($_POST['id']) && isset($_POST['id'])) {
            $a = Article::GetByID($_POST['id']);

            $dbg = Article::EditDetails(
                $a->id,
                $_POST['category'],
                $_POST['tags'],
                $_POST['author']
            );
        }
    } else {
        die('Unauthorized access');
    }
}

// Load up Twig stuff
require dirname(__DIR__, 4).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$token = Token::Get(64);
$_SESSION['token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('admin/managers/matrix.twig', array(
        'theme'      => $theme = $_COOKIE['theme'] ?? 'light',
        'user'       => !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null,
        'discord_users' => Discord::Online(),

        'token'      => $token,
        'articles'   => Article::GetAll(),
        'categories' => Category::GetAll(),
        'tags'       => Tag::GetAll(),
        'users'      => User::GetAll(),
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}

