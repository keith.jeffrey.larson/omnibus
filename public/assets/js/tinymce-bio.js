tinymce.init({
    selector: '#editor',

    autoresize_max_height: 600,
    autoresize_min_height: 300,

    plugins: "autoresize textcolor colorpicker hr link nonbreaking paste anchor contextmenu image " +
             "lists preview searchreplace table autolink fullscreen media spellchecker",

    menubar: "file edit view insert",

    toolbar: "removeformat | styleselect fontsizeselect | forecolor | bold italic underline strikethrough | blockquote | alignleft aligncenter alignright alignjustify | " +
             "outdent indent| numlist bullist | link image table | fullscreen spellchecker",

    contextmenu: "copy paste | bold italic underline strikethrough | alignleft aligncenter alignright",

    branding: false,
    default_link_target: "_blank",
    link_assume_external_targets: true,

    target_list: [
        {title: 'None', value: ''},
        {title: 'Same page', value: '_self'},
        {title: 'New page', value: '_blank'}
    ],

    contextmenu_never_use_native: true,

    image_caption: true,
    image_class_list: [
        {title: 'Fluid', value: 'ui fluid image'},
        {title: 'Centered', value: 'ui centered image'},
        {title: 'Rounded', value: 'ui rounded image'},
        {title: 'Circular', value: 'ui circular image'}
    ],

    plugin_preview_height: 600,
    plugin_preview_width: 1000,

    fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt 48pt"
});
