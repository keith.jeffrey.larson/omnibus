<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 29.09.2018
 * Time: 07:21
 */

if(!empty($_SESSION['userid'])) {
    header('Location: /');
    die();
}

require dirname(__DIR__, 3).'/backend/Captcha.php';

// Get URL data for redirect
$domain = Settings::Read('domain');

// RESTORE USER
$error = false;
$message = array();
if (!empty($_POST) && !empty($_SESSION)) {

    // Validate tokens
    if ($_POST['token'] !== $_SESSION['token']) {
        die('Access this form from website only.');
    }

    // Check $_POST keys
    $whitelist = array('login', 'email', 'token', 'g-recaptcha-response');
    foreach ($_POST as $key=>$item) {
        if (!in_array($key, $whitelist, false)) {
            die('Please use only the fields in the form.');
        }
    }
// check secret key
    $captcha = new Captcha(Settings::Read('captcha-secret'));
    $response = $captcha->Verify($_POST['g-recaptcha-response']);

    if ($response === true) {

        // Check if fields aren't empty
        if (empty($_POST['login'])) {
            $error = true;
            $message['body'] .= '⚠ Empty login<br>';
        }
        if (empty($_POST['email'])) {
            $error = true;
            $message['body'] .= '⚠ Empty email<br>';
        }

        // Check if email is valid
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $error = true;
            $message['body'] .= '⚠ Invalid email address<br>';
        }

        // Check if user exists
        $l_user = User::GetByName($_POST['login']);
        $e_user = User::GetByEmail($_POST['email']);
        if ($l_user === null || $e_user === null || $e_user->id !== $l_user->id) {
            $error = true;
            $message['body'] .= '⚠ No such user<br>';
        }

        if (!$error) {

            // Get user
            $user = User::GetByEmail($_POST['email']);

            // Get pass
            $pass = Token::Get(16);
            // Encrypt it
            $safe_pass = password_hash($pass, PASSWORD_BCRYPT);

            $sql = 'UPDATE `users`
            SET `password` = :pass
            WHERE `ID_user` = :id';

            $sth = Database::Get()->prepare($sql);

            $sth->bindParam(':id', $user->id);
            $sth->bindParam(':pass', $safe_pass);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                $message['body'] .= '⚠ Database error<br>';
            }

            $body = file_get_contents('assets/emails/restore.html');
            $body = str_replace(array('{{user}}', '{{password}}'), array($_POST['login'], $pass), $body);
            $subject = 'New password for School for New Writers';
            $headers = @"From: \"The Dean of SFNW\" <dean@sfnw.online>\r\n" .
                       @"Reply-To: \"The Dean of SFNW\" <dean@sfnw.online>\r\n" .
                       @'X-Mailer: PHP/' . PHP_VERSION .
                       @"MIME-Version: 1.0\r\n" .
                       @"Content-Type: text/html; charset=ISO-8859-1\r\n";

            try {
                mail($_POST['email'], $subject, $body, $headers);
            } catch (Exception $e) {
                $message['body'] .= '⚠ Emailer error<br>';
            }

            header('Location: /login?msg=restored');
        }

    } else {
        // redirect if captcha incorrect
        $message['body'] .= '⚠ Incorrect Captcha<br>';
    }
}

// Messages
if ($_GET['msg'] === 'activated') {
    $message = array(
        'type'   => 'success',
        'header' => 'Nice!',
        'body'   => 'Your account is now active – all that\'s left to do is log in!',
    );
}

// Load up Twig stuff
$loader = new Twig_Loader_Filesystem(array(
        dirname(__DIR__,2).'/views',
        dirname(__DIR__,2).'/assets')
);
$twig = new Twig_Environment($loader);

// Set up variables
$token = Token::Get(64);
$_SESSION['token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('/user/restore.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null,
        'discord_users' => Discord::Online(),
        'quote'         => Quote::GetRandomQuote(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),

        'token'   => $token,
        'message'  => $message,
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}
