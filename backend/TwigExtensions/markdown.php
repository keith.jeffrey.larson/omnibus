<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.10.2018
 * Time: 06:53
 */

/** @var $t_parse_markdown Twig_SimpleFilter */
$t_parse_markdown = new Twig_SimpleFilter('md', static function (string $string) {
    $Extra = new ParsedownExtra();
    return $Extra->text($string);
});
