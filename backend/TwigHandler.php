<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 02.11.2018
 * Time: 22:36
 */


class TwigHandler
{
    public static function Get(): Twig_Environment
    {
        require __DIR__.'/TwigExtensions/markdown.php';
        require __DIR__.'/TwigExtensions/links.php';
        require __DIR__.'/TwigExtensions/shelves.php';

        $loader = new Twig_Loader_Filesystem(array(
                dirname(__DIR__,1).'/public/views',
                dirname(__DIR__,1).'/public/assets')
        );
        $twig = new Twig_Environment($loader);

        $twig->addFilter($t_parse_markdown);

        $twig->addExtension(new Twig_Extensions_Extension_Date());

        $twig->addFunction($t_get_icon);
        $twig->addFunction($t_get_name);
        $twig->addFunction($t_check_bookmark);
        $twig->addFunction($t_check_favourite);
        $twig->addFunction(new \Twig_SimpleFunction('asset', static function ($asset) {
            return sprintf('/public/%s', ltrim($asset, '/'));
        }));

        return $twig;
    }
}
