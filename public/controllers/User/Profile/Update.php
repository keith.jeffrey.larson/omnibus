<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 26.10.2018
 * Time: 03:18
 */

require dirname(__DIR__, 4) . '/backend/Avatar.php';

// Load up Twig stuff
require dirname(__DIR__, 4) . '/backend/TwigExtensions/links.php';
$loader = new Twig_Loader_Filesystem(array(
        dirname(__DIR__, 3) . '/views',
        dirname(__DIR__, 3) . '/assets')
);
$twig = new Twig_Environment($loader);
$twig->addExtension(new Twig_Extensions_Extension_Date());
$twig->addFunction(new \Twig_SimpleFunction('asset', static function ($asset) {
    return sprintf('/public/%s', ltrim($asset, '/'));
}));

// Message
$message = '';

// EDIT PROFILE
if (isset($_POST['type']) && $_POST['token'] === $_SESSION['token']) {
    $u = User::GetByID($_POST['id']);
    switch ($_POST['type']) {
        case 'profile':
            $path = $_FILES['file']['error'] !== 0 ? null : Avatar::Upload($_POST['id'], $_POST['token'], $_FILES['file']);
            // Modify user
            $u->EditProfile($_POST['title'], $_POST['link'], $path);
            break;

        case 'bio':
            $u->EditBio($_POST['bio']);
            break;

        case 'account':
            if (!password_verify($_POST['password'], $u->password)) {
                $message = 'Current password is incorrect';
            } else if ($_POST['newpass1'] !== $_POST['newpass2']) {
                $message = 'New passwords do not match';
            } else {
                $u->EditAccount($_POST['email'], $_POST['newpass1']);
            }
            break;
    }
}

// Set up variables
$token = Token::Get(64);
$_SESSION['token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('/user/profile/update.twig', array(
        'theme' => $theme = $_COOKIE['theme'] ?? 'light',
        'user' => !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null,
        'discord_users' => Discord::Online(),
        'classes' => Classroom::GetAll(),
        'tags' => Tag::GetAll(),
        'categories' => Category::GetAll(),
        'authors' => User::GetAllByRole(false, true, true),

        'token' => $token,
    ));

    // Handle all possible errors
} catch (Twig_Error_Loader $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: ' . $e);
} catch (Twig_Error_Runtime $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: ' . $e);
} catch (Twig_Error_Syntax $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: ' . $e);
} catch (Exception $e) {
    header('Content-type: application/json');
    echo json_encode('Error [4]: ' . $e);
}
