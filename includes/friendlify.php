<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 31.07.2018
 * Time: 01:20
 * @param $str
 * @return null|string|string[]
 */

// FRIENDLIFY
function friendlify($str) {
    $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
    $str = trim($str, '-');
    $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);
    $str = strtolower($str);
    $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
    return $str;
}
