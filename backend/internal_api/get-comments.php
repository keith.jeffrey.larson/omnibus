<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 01.12.2018
 * Time: 05:35
 */

use Omnibus\Comment;

session_name('__Secure-PHPSESSID');
session_set_cookie_params(
    0,
    '/',
    'sfnw.online',
    true,
    true
);
session_start();

require dirname(__DIR__, 1).'/models/Comment.php';
$comments = Comment::GetAll(true, $_GET['thread']);

//header('HTTP/2.0 200 OK');
header('Content-Type: text/html');
//echo '<pre>'.var_export($comments, true).'</pre>';

foreach ($comments as $c) {

    if($c->reported_by) {
        $body = '<pre class="reported-comment">pending moderation</pre>';
        $report = '';
    } else {
        $body = $c->body;
        $report = "<div data-tooltip='report' data-id='{$c->id}' class='report-btn'><i class='exclamation triangle icon'></i></div>";
    }

    // Check if user is even logged in
    if (empty($_SESSION['userid'])) {
        $report = '';
    }

    echo "<div class='ui comment'>";
    echo "<a class='avatar'>";
    echo "<img alt='avatar' class='ui avatar image' src='{$c->author_avatar}'>";
    echo '<a>';
    echo "<div class='content'>";
    echo "<a class='author'>{$c->author}</a>";
    echo "<div class='metadata'>";
    echo "<div class='date s-date'>{$c->date}</div>";
    echo $report;
    echo '</div>';
    echo "<div class='text'>{$body}</div>";
    echo '</div>';
    echo '</div>';
}
die();
