<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 04:48
 */

use Fuse\Fuse;

require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

// GET DATABASE
require_once __DIR__ . '/Database.php';

class Tag {
    public $id;
    public $name;
    public $furl;
    public $description;
    public $picture;

    /**
     * Tag constructor.
     * @param $id
     * @param $name
     * @param $description
     * @param $picture
     * @param null $furl
     */
    public function __construct($id, $name, $description, $picture, $furl = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->furl = $furl ?? self::friendlify($name);
        $this->description = $description;
        $this->picture = $picture;
    }


    /**
     * Gets all tags assigned to a selected article
     * @param int $art Takes an Integer that is the desired **article** ID
     * @return array|string Returns an array of tags or a string with an error
     */
    public static function GetByArticle(int $art) {
        $dbh = Database::Get();

        $sql = 'SELECT `tags`.*, `article_has_tags`.*
                    FROM `article_has_tags`
                        JOIN `tags` ON `article_has_tags`.`tags_ID_tag` = `tags`.`ID_tag`
                    WHERE `tags_ID_article` = :article 
                    ORDER BY `name_tag` ASC';

        $sth = $dbh->prepare($sql);
        $sth->bindParam(':article', $art);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [11]' . $e->getMessage() . '<br/>';
        }

        $tags = $sth->fetchAll();

        foreach ($tags as $key => $t) {
            $tags[$key] = self::Build($t);
        }
        return $tags;
    }

    /**
     * Gets all tags that exist in the database
     * @param int $limit Takes a limit of how many results should be returned
     * @return array|string Returns an array of tags or a string with an error
     */
    public static function GetAll(int $limit = 0) {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM `tags` ORDER BY `name_tag` ASC';

        // Add limit if necessary
        $sql = $limit > 0 ? $sql.' LIMIT :l' : $sql;
        $li = (int)trim($limit);

        $sth = $dbh->prepare($sql);

        if($limit > 0) {
            $sth->bindParam('l', $li, PDO::PARAM_INT);
        }

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [12]' . $e->getMessage() . '<br/>';
        }

        $tags = $sth->fetchAll();

        foreach ($tags as $key => $t) {
            $tags[$key] = self::Build($t);
        }
        return $tags;
    }

    /**
     * Gets tag by the specified ID number
     * @param int $id Takes an Integer that is the desired tag ID
     * @return Tag|string Returns a Tag, or a String with an error
     */
    public static function GetByID(int $id) {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM `tags` WHERE `ID_tag` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [13]' . $e->getMessage() . '<br/>';
        }

        $tag = $sth->fetch(PDO::FETCH_ASSOC);
        return self::Build($tag);
    }

    /**
     * Gets tags with titles that match the query
     * @param string $query Takes a query to match the results against
     * @param int $limit Takes a number of results to return
     * @return array Returns an array of results, ordered by how well they match
     */
    public static function Search(string $query, int $limit = 5): array
    {
        $tags = json_decode(json_encode(self::GetAll()), true);

        $fuse = new Fuse($tags,
            [
                'keys' => ['name'],
                'includeScore' => true,
            ]
        );

        $r = $fuse->search($query);
        $rl = $limit > 0 ? array_chunk($r, $limit)[0] : $r;

        $out = [];
        foreach ($rl as $tag) {
            $out[] = array(
                'id'          => $tag['item']['id'],
                'name'        => $tag['item']['name'],
                'description' => $tag['item']['description'],
                'picture'     => $tag['item']['picture'],
                'furl'        => 'https://sfnw.online/t/'.$tag['item']['id'].'/'.$tag['item']['furl'],
                'similarity'  => $tag['score'],
            );
        }

        return $out;
    }


    /**
     * Deletes tag and its associations with articles from database
     * @param $id int ID of the tag to be deleted
     * @return null|string Returns null if successful or an error string
     */
    public static function Delete(int $id): ?string
    {
        $dbh = Database::Get();
        $sql = 'DELETE FROM `article_has_tags` WHERE `tags_ID_tag` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        $sql = 'DELETE FROM `tags` WHERE `ID_tag` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        return null;
    }

    /** Adds Tag to database
     * @param string $name
     * @param string $description
     * @param string $image
     * @return null|string
     */
    public static function Add(string $name, string $description, string $image): ?string
    {
        $dbh = Database::Get();
        // Prepare image placeholder
        $img = empty($image) ? 'https://sfnw.online/public/assets/img/placeholder.png' : $image;

        // If everything's fine, add this shit to the database
        $sql = 'INSERT INTO `tags` (name_tag, furl_tag, description_tag, picture_tag) 
                VALUES (:name, :furl, :desc, :picture)';

        $sth = $dbh->prepare($sql);

        $furl = self::friendlify($name);
        $sth->bindParam(':name', $name);
        $sth->bindParam(':furl', $furl);
        $sth->bindParam(':desc', $description);
        $sth->bindParam(':picture', $img);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        return null;
    }

    /**
     * Edits tag
     * @param int $id
     * @param string $name
     * @param string $description
     * @param string $image
     * @return null|string
     */
    public static function Edit(int $id, string $name, string $description, string $image): ?string
    {
        $dbh = Database::Get();
        // If everything's fine, add this shit to the database
        $sth = $dbh->prepare('UPDATE `tags` 
                                            SET `name_tag` = :name,
                                                `furl_tag` = :furl,
                                                `description_tag` = :desc,
                                                `picture_tag` = :picture
                                            WHERE `ID_tag` = :id');

        $furl = self::friendlify($name);
        $sth->bindParam(':name', $name);
        $sth->bindParam(':furl', $furl);
        $sth->bindParam(':desc', $description);
        $sth->bindParam(':picture', $image);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        return null;
    }

    /**
     * Counts tags currently in database
     * @return int|string Returns an Integer equal to the amount of tags, or a String with an error
     */
    public static function Count() {
        $dbh = Database::Get();
        $sql = 'SELECT COUNT(*) FROM `tags`';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }

        return (int)$sth->fetchColumn();
    }

    /**
     * Friendlifies the name
     * @param string $str String to be friendlified
     * @return null|string Friendlified string
     */
    private static function friendlify($str): ?string
    {
        $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
        $str = trim($str, '-');
        $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
        return $str;
    }

    /**
     * Transforms an assoc array returned from database query into a Tag object
     * @param array $tag Takes an assoc array describing a Tag
     * @return Tag returns a built Tag object
     */
    protected static function Build(array $tag): Tag
    {
        $t = new Tag(
            $tag['ID_tag'],
            $tag['name_tag'],
            $tag['description_tag'],
            $tag['picture_tag']
        );
        return $t;
    }

    /**
     * @return Tag
     */
    public function Parse(): Tag
    {
        $Parsedown = new Parsedown();
        return new Tag(
            $this->id,
            $this->name,
            $Parsedown->text($this->description),
            $this->picture
        );
    }

    /**
     * @param array[Tag] $tags
     * @return array
     */
    public static function ParseAll(array $tags): array
    {
        $out = array();
        foreach ($tags as $tag) {
            $out[] = $tag->Parse();
        }
        return $out;
    }
}
